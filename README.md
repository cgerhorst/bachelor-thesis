# Bachelor Thesis: "Spin-Boson-Model with a coherent basis"

written by Christian-Roman Gerhorst.

PDFs in directory `final_versions`.

## Required programs (tested versions):
* GNU make 3.82
* GCC 4.8.1
* TeXLive 2013
* Python 3.3.2

## C++ linear algebra library:
* eigen 3.2.0

## Python libraries (Python 3):
* numpy 1.7.1
* matplotlib 1.3.0 (patched: <https://github.com/matplotlib/matplotlib/pull/2402>)

## Used languages
* Thesis written in Latex
* Source code for calculations written in C++ using Eigen
* Scripts for plotting written in Python using numpy and matplotlib
