#include <complex>
#include <iostream>
#include <cmath>
#include <string>
#include <cstdio>

/*
 * Eigen for linear algebra
 */

#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/KroneckerProduct>

/*
 * omp for paralellization
 */
#include <omp.h>

/*
 * expected value for state l and operatore op
 */
#define expVF(l, op) (l.adjoint() * (op * l))(0,0)

using Eigen::Vector2cd;
using Eigen::Matrix2cd;
using Eigen::MatrixXcd;
using std::complex;

typedef Matrix2cd MatrixS;

MatrixS sigmaX;
MatrixS sigmaZ;

/*
 * define Pauli matrices
 */
void createPauli()
{
	sigmaX << 0, 1,
			  1, 0;

	sigmaZ << 1,  0,
			  0, -1;
}

/*
 * create bosonic Ladder operator
 */
MatrixXcd createLadderOpF(bool dagger, MatrixXcd::Index oModes)
{
	MatrixXcd b(oModes, oModes);
	MatrixXcd::Index i, j;

	for (i = 0; i < b.rows(); i++)
	{
		for(j = 0; j < b.cols(); j++)
		{
			b(i, j) = (i == j - 1) ? sqrt(j): 0;
		}
	}

	if (dagger)
	{
		b.adjointInPlace();
	}

	return b;
}

/*
 * build spin hamiltonian matrix for given bias epsilon and tunneling amplitude delta
 */
MatrixS buildHamiltonianS(double epsilon, double delta)
{
	createPauli();
	return - delta / 2 * sigmaX + epsilon / 2 * sigmaZ;
}


/*
 * build bosonic hamiltonian matrix for given frequency omega and oModes states
 */
MatrixXcd buildHamiltonianB(double omega, MatrixXcd::Index oModes)
{
	MatrixXcd b = createLadderOpF(false, oModes);
	MatrixXcd b_dagger = createLadderOpF(true, oModes);

	return omega * b_dagger * b;
}

/*
 * build coupling hamiltonian matrix for given coupling lambda and oModes states
 */
MatrixXcd buildHamiltonianC(double lambda, MatrixXcd::Index oModes)
{
	MatrixXcd b = createLadderOpF(false, oModes);
	MatrixXcd b_dagger = createLadderOpF(true, oModes);
	MatrixXcd H_C(2 * oModes, 2 * oModes);
	MatrixXcd x(oModes, oModes);

	x = b + b_dagger;
	H_C = kroneckerProduct(sigmaZ, x);

	return lambda / 2 * H_C;
}

/*
 * make x-operator matrix for oModes states
 */
MatrixXcd makeXOp(MatrixXcd::Index oModes)
{
	MatrixXcd b = createLadderOpF(false, oModes);
	MatrixXcd b_dagger = createLadderOpF(true, oModes);

	return b + b_dagger;
}

/*
 * make x²-operator matrix for oModes states
 */
MatrixXcd makeXSqOp(MatrixXcd::Index oModes)
{
	MatrixXcd b = createLadderOpF(false, oModes);
	MatrixXcd b_dagger = createLadderOpF(true, oModes);
	MatrixXcd ident = MatrixXcd::Identity(oModes, oModes);

	return b * b + b_dagger * b_dagger + 2 * b_dagger * b + ident;
}

/*
 * testing routines
 */
//template <typename Derived>
//MatrixXcd calcErrorR(MatrixXcd ana, const Eigen::MatrixBase<Derived> &num, MatrixXcd::Index dim)
//{
//	using std::abs;
//
//	MatrixXcd err(dim, dim);
//
//	MatrixXcd::Index i, j;
//
//	#pragma omp parallel private(i, j)
//	{
//		#pragma omp for schedule(dynamic, CHUNKSIZE)
//		for(i = 0; i < dim; i++)
//		{
//			for(j = 0; j < dim; j++)
//			{
//				err(i,j) = abs(num(i,j) - ana(i,j)) / abs(ana(i, j));
//			}
//		}
//	}
//
//	return err;
//}
//
//template <typename Derived>
//MatrixXcd calcErrorA(MatrixXcd ana, const Eigen::MatrixBase<Derived> &num, MatrixXcd::Index dim)
//{
//	using std::abs;
//
//	MatrixXcd err(dim, dim);
//
//	MatrixXcd::Index i, j;
//
//	#pragma omp parallel private(i, j)
//	{
//		#pragma omp for schedule(dynamic, CHUNKSIZE)
//		for(i = 0; i < dim; i++)
//		{
//			for(j = 0; j < dim; j++)
//			{
//				err(i,j) = abs(num(i,j) - ana(i,j));
//			}
//		}
//	}
//
//	return err;
//}
//
//void calcError (complex<double> num[], complex<double> ana[], unsigned int number, complex<double> *abs_err, complex<double> *rel_err)
//{
//	using std::abs;
//
//	#pragma omp parallel for schedule(dynamic, CHUNKSIZE)
//	for (size_t i = 0; i < number; i++)
//	{
//		abs_err[i] = abs(num[i] - ana[i]);
//		rel_err[i] = abs_err[i] / abs(ana[i]);
//	}
//
//}

///*
// * print the first "number" analytic eigenvalues "energ" and numeric eigenvalues "n_ev"
// */
//template <typename Derived>
//void printEnergies(std::complex<double> *energ, unsigned int number, const Eigen::MatrixBase<Derived> &n_ev)
//{
//	using std::cout;
//	using std::endl;

//	cout << "analytisch:" << endl;
//	for(unsigned int i = 0; i < number; i++)
//	{
//		cout << "E_" << i << "_a = " << energ[i] << endl;
//	}
//	cout << "numerisch:" << endl;
//	for (typename Eigen::MatrixBase<Derived>::Index i = 0; i < n_ev.rows(); i ++)
//	{
//		cout << "E_" << i << "_n = " << (complex<double>) n_ev(i) << endl;
//	}
//
//}

///*
// * print absolute and relative errors of the energies
// */
//void printEnErrors (complex<double> babs_err[], complex<double> brel_err[], unsigned int oModes)
//{

//	using std::cout;
//	using std::endl;

//	cout << "absoluter Fehler" << endl;

//	for (size_t i = 0; i < oModes; i++)
//	{
//		cout << "ΔE_" << i << "_abs = " << babs_err[i] << endl;
//	}

//	cout << "relativer Fehler" << endl;

//	for (size_t i = 0; i < oModes; i++)
//	{
//		cout << "ΔE_" << i << "_rel = " << brel_err[i] << endl;
//	}

//}

/*
 * end of testing routine blocks
 */

void unshiftedFockStates(double delta, double epsilon, double lambda, double omega, size_t oModes, std::string filename)
{
	using std::cout;
	using std::endl;
	using std::abs;
	using std::complex;
/*
 * testing and io-routines are commented out within (not all tested)
 */

/*
 * spin-hamiltonian
 */

//	cout << "H_Spin" << endl;
//	cout << "Δ = " << delta << ", ε = " << epsilon << endl << endl;

	MatrixS H_S = buildHamiltonianS(epsilon,delta);

//	cout << "H_S = " << endl << H_S << endl << endl;

	// analytic eigenvalues

	complex<double> sE_a [2];
	sE_a[0] = - sqrt(epsilon * epsilon + delta * delta) / 2.0;
	sE_a[1] = -sE_a[0];

	//numeric eigenvalues
	Eigen::SelfAdjointEigenSolver<MatrixS> d_H_S(H_S);
	complex<double> sE_n [2];
	sE_n[0] = d_H_S.eigenvalues()(0);
	sE_n[1] = d_H_S.eigenvalues()(1);

//	cout << "Eigenwerte:" << endl;
//	printEnergies(sE_a, 2, d_H_S.eigenvalues());

//	complex<double> *srel_err = new complex<double>[2];
//	complex<double> *sabs_err = new complex<double>[2];
//	calcError(sE_n, sE_a, 2, sabs_err, srel_err);

//	cout << "absoluter Fehler" << endl;
//	cout << "ΔE_1_abs = " << sabs_err[0] << endl;
//	cout << "ΔE_2_abs = " << sabs_err[1] << endl;
//	cout << "relativer Fehler" << endl;
//	cout << "ΔE_1_rel = " << srel_err[0] << endl;
//	cout << "ΔE_2_rel = " << srel_err[1] << endl;

	// analytic and numeric eigenvectors

	MatrixS s_ev;
//	MatrixS s_del_ev_r;
//	MatrixS s_del_ev_a;
	// calculating analytic coefficients α_+, α_-, β_+, β_-
	complex<double> alpha_p;
	complex<double> alpha_m;
	complex<double> beta_p;
	complex<double> beta_m;

	alpha_p = delta / sqrt(delta * delta + pow((epsilon - sqrt(epsilon * epsilon + delta * delta)), 2));
	beta_p = (-epsilon + sqrt(epsilon * epsilon + delta * delta)) / sqrt(delta * delta + pow((epsilon - sqrt(epsilon * epsilon + delta * delta)), 2));
	alpha_m = delta / sqrt(delta * delta + pow((epsilon + sqrt(epsilon * epsilon + delta * delta)), 2));
	beta_m = (-epsilon - sqrt(epsilon * epsilon + delta * delta)) / sqrt(delta * delta + pow((epsilon + sqrt(epsilon * epsilon + delta * delta)), 2));

	s_ev(0, 0) = alpha_p;
	s_ev(1, 0) = beta_p;
	s_ev(0, 1) =  alpha_m;
	s_ev(1, 1) =  beta_m;

	//calculating errors

//	for(MatrixS::Index i = 0; i < 2; i++)
//	{
//		for(MatrixS::Index j = 0; j < 2; j++)
//		{
//			s_del_ev_a(i, j) = abs(s_ev(i,j) - d_H_S.eigenvectors()(i,j));
//			s_del_ev_r(i, j) = s_del_ev_a(i, j) / abs(s_ev(i, j));
//		}
//	}

////cout << endl << "Eigenvektoren" << endl;
////cout << "analytische" << endl << s_ev << endl;
////cout << "numerisch" << endl << d_H_S.eigenvectors() << endl;
////cout << "absolute Fehlermatrix" << endl << s_del_ev_a << endl;
////cout << "relative Fehlermatrix" << endl << s_del_ev_r << endl;

/*
 * bosonic hamiltonian
 */

	MatrixXcd H_B = buildHamiltonianB(omega, oModes);

//	cout << endl << endl << "H_Bosonen" << endl << endl;
//	cout << "H_B = " << endl << H_B << endl << endl;

	// analytic eigenvalues

//	cout << "Eigenwerte" << endl;


	complex<double> *bE_a = new complex<double>[oModes];

	for(unsigned int i = 0; i < oModes; i++)
	{
		bE_a[i] = i;
	}

	//numeric eigenvalues

	Eigen::SelfAdjointEigenSolver<MatrixXcd> d_H_B(H_B);

//	printEnergies(bE_a, oModes, d_H_B.eigenvalues());

//	complex<double> *brel_err = new complex<double>[oModes];
//	complex<double> *babs_err = new complex<double>[oModes];
//	calcError(d_H_B.eigenvalues(), bE_a, oModes, babs_err, brel_err);

//	printEnErrors(babs_err, brel_err, oModes);

	// analytic eigenvectors
	MatrixXcd b_ev = MatrixXcd::Identity(oModes, oModes);
//	cout << endl << "analytische Eigenvektoren" << endl << b_ev << endl;

	// numeric eigenvectors
//	MatrixXcd b_del_ev_r = MatrixXcd::Zero(oModes, oModes);
//	MatrixXcd b_del_ev_a = MatrixXcd::Zero(oModes, oModes);

//	b_del_ev_a = calcErrorA(b_ev, d_H_B.eigenvectors(), oModes);
//	b_del_ev_r = calcErrorR(b_ev, d_H_B.eigenvectors(), oModes);

//	cout << "numerische Eigenvektoren" << endl << d_H_B.eigenvectors() << endl;
//	cout << "absolute Fehlermatrix" << endl << b_del_ev_a << endl;
//	cout << "relative Fehlermatrix" << endl << b_del_ev_r << endl;

/*
 * H_SPIN-BOSON
 */

//	cout << endl << endl << "H_Spin_Bosonen" << endl;

	MatrixXcd H_SB_B(2 * oModes, 2 * oModes);
	MatrixXcd H_SB_S(2 * oModes, 2 * oModes);
	MatrixXcd H_SB_C(2 * oModes, 2 * oModes);
	MatrixXcd H_B_I = MatrixXcd::Identity(oModes, oModes);
	MatrixS H_S_I = MatrixS::Identity();
	MatrixXcd H_SB_SB(2 * oModes, 2 * oModes);

	H_SB_C = buildHamiltonianC(lambda, oModes);

	H_SB_S = kroneckerProduct(H_S, H_B_I);
	H_SB_B = kroneckerProduct(H_S_I, H_B);

	H_SB_SB = H_SB_B + H_SB_S + H_SB_C;

//	cout << endl << H_SB_SB << endl;

	// analytic eigenvalues with correcture and numerical version

	Eigen::SelfAdjointEigenSolver<MatrixXcd> d_H_SB_SB(H_SB_SB);

	complex<double> *sbE_a = new complex<double> [4];
	complex<double> *sbE_n = new complex<double> [4];

	complex<double> tmp_p = lambda / 2.0 * (alpha_p * alpha_p - beta_p * beta_p);
	complex<double> tmp_m = lambda / 2.0 * (alpha_m * alpha_m - beta_m * beta_m);

//	complex<double> sbE_corr_p = abs(tmp_p) * abs(tmp_p) / omega;
//	complex<double> sbE_corr_m = abs(tmp_m) * abs(tmp_m) / omega;

	complex<double> eta_p = lambda / 2.0 * (alpha_p * alpha_p - beta_p * beta_p);
	complex<double> eta_m = lambda / 2.0 * (alpha_m * alpha_m - beta_m * beta_m);

	complex<double> zeta = lambda / 2.0 * (alpha_p * alpha_m - beta_p * beta_m);

	complex<double> sbE_corr_0 = abs(eta_m) * abs(eta_m) / omega + abs(zeta) * abs(zeta) / (sqrt(epsilon * epsilon + delta * delta) + omega);
	complex<double> sbE_corr_1 = abs(eta_p) * abs(eta_p) / omega + abs(zeta) * abs(zeta) / (omega - sqrt(epsilon * epsilon + delta * delta));

	sbE_a[0] = sE_a[0] + bE_a[0] - sbE_corr_0;
//	sbE_a[1] = sE_a[0] + bE_a[1] + sbE_corr_p;
	sbE_a[2] = sE_a[1] + bE_a[0] - sbE_corr_1;
//	sbE_a[3] = sE_a[1] + bE_a[1] + sbE_corr_m;

	sbE_n[0] = d_H_SB_SB.eigenvalues()(0);
//	sbE_n[1] = d_H_SB_SB.eigenvalues()(2);
	sbE_n[2] = d_H_SB_SB.eigenvalues()(1);
//	sbE_n[3] = d_H_SB_SB.eigenvalues()(3);

	//cout << endl << "Eigenwerte" << endl;
	//cout << "analytisch" << endl;
	//cout << "E_+_0_a = " << sbE_a[0] << endl;
	//cout << "E_-_0_a = " << sbE_a[2] << endl;
	//cout << "E_+_1_a = " << sbE_a[1] << endl;
	//cout << "E_-_1_a = " << sbE_a[3] << endl;
	//cout << "numerisch" << endl;
	//cout << "E_+_0_n = " << sbE_n[0] << endl;
	//cout << "E_-_0_n = " << sbE_n[2] << endl;
	//cout << "E_+_1_n = " << sbE_n[1] << endl;
	//cout << "E_-_1_n = " << sbE_n[3] << endl;

	//cout << "all numerical eigenvalues" << endl << d_H_SB_SB.eigenvalues() << endl;

	//calculating errors

//	complex<double> *sbabs_err = new complex<double> [4];
//	complex<double> *sbrel_err = new complex<double> [4];

//	calcError(sbE_n, sbE_a, 4, sbabs_err, sbrel_err);

	//cout << "absoluter Fehler" << endl;
	//cout << "ΔE_+_0 = " << sbabs_err[0] << endl;
	//cout << "ΔE_+_1 = " << sbabs_err[1] << endl;
	//cout << "ΔE_-_0 = " << sbabs_err[2] << endl;
	//cout << "ΔE_-_1 = " << sbabs_err[3] << endl;
	//cout << "relativer Fehler" << endl;
	//cout << "ΔE_+_0 = " << sbrel_err[0] << endl;
	//cout << "ΔE_+_1 = " << sbrel_err[1] << endl;
	//cout << "ΔE_-_0 = " << sbrel_err[2] << endl;
	//cout << "ΔE_-_1 = " << sbrel_err[3] << endl;

	// analytic eigenvectors with correcture and output of numerical and analytic version

	MatrixXcd ev_a_SB (2 * oModes, 2 * oModes);
	MatrixXcd ev_a_SB_rel (2 * oModes, 2 * oModes);
	MatrixXcd ev_a_SB_abs (2 * oModes, 2 * oModes);

	ev_a_SB = kroneckerProduct(d_H_S.eigenvectors(), d_H_B.eigenvectors());

//	cout << endl << "Eigenvektoren" << endl;
//	cout << "analytisch vor Störung" << endl << ev_a_SB << endl;

	tmp_p /= omega;
	tmp_m /= omega;
	ev_a_SB.block(0, 0, 2 * oModes, 1) +=  tmp_p * ev_a_SB.block(0, 1, 2 * oModes, 1);
	ev_a_SB.block(0, 1, 2 * oModes, 1) -= tmp_p * ev_a_SB.block(0, 0, 2 * oModes, 1);
	ev_a_SB.block(0, oModes, 2 * oModes, 1) += tmp_m * ev_a_SB.block(0, oModes + 1, 2 * oModes, 1);
	ev_a_SB.block(0, oModes + 1, 2 * oModes, 1) -= tmp_m * ev_a_SB.block(0, oModes, 2 * oModes, 1);


//	double norm_p = sqrt(1 + tmp_p.real() * tmp_p.real());
//	double norm_m = sqrt(1 + tmp_m.real() * tmp_m.real());

	complex<double> tmp_0 = sqrt(epsilon * epsilon + delta * delta) + omega;
	complex<double> tmp_1 = omega - sqrt(epsilon * epsilon + delta * delta);

	double norm_0_sqr = pow(1 + abs(eta_m * eta_m / omega / omega + zeta * zeta / tmp_0 / tmp_0), -1);
	double norm_1_sqr = pow(1 + abs(eta_p * eta_p / omega / omega + zeta * zeta / tmp_1 / tmp_1), -1);

//	cout << "analytisch mit Störung:" << endl;
//	cout << ev_a_SB << endl;
//	cout << "numerisch:" << endl;
//	cout << d_H_SB_SB.eigenvectors() << endl;

	//calculating errors

//	ev_a_SB_abs = calcErrorA(ev_a_SB, d_H_SB_SB.eigenvectors(), 4);
//	ev_a_SB_rel = calcErrorR(ev_a_SB, d_H_SB_SB.eigenvectors(), 4);
//
//	cout << "absoluter Fehler der Eigenvektoren" << endl;
//	cout << ev_a_SB_abs << endl;
//	cout << "relativer Fehler der Eigenvektoren" << endl;
//	cout << ev_a_SB_rel << endl;

/*
 * expectation values <x> and <x²>
 */

	MatrixXcd x = makeXOp(oModes);
	MatrixXcd x_sq = makeXSqOp(oModes);
	MatrixXcd x_SB (2 * oModes, 2 * oModes);
	MatrixXcd x_sq_SB (2 * oModes, 2 * oModes);
	complex<double> exp_x_0_p_a ;
	complex<double> exp_x_0_m_a;
//	complex<double> exp_x_1_p_a;
//	complex<double> exp_x_1_m_a;
	complex<double> exp_x_0_p_n;
	complex<double> exp_x_0_m_n;
//	complex<double> exp_x_1_p_n;
//	complex<double> exp_x_1_m_n;
//	complex<double> exp_x_0_p_re;
//	complex<double> exp_x_0_m_re;
//	complex<double> exp_x_1_p_re;
//	complex<double> exp_x_1_m_re;
//	complex<double> exp_x_0_p_ae;
//	complex<double> exp_x_0_m_ae;
//	complex<double> exp_x_1_p_ae;
//	complex<double> exp_x_1_m_ae;
	complex<double> exp_x_sq_0_p_a;
	complex<double> exp_x_sq_0_m_a;
//	complex<double> exp_x_sq_1_p_a;
//	complex<double> exp_x_sq_1_m_a;
	complex<double> exp_x_sq_0_p_n;
	complex<double> exp_x_sq_0_m_n;
//	complex<double> exp_x_sq_1_p_n;
//	complex<double> exp_x_sq_1_m_n;
//	complex<double> exp_x_sq_0_p_re;
//	complex<double> exp_x_sq_0_m_re;
//	complex<double> exp_x_sq_1_p_re;
//	complex<double> exp_x_sq_1_m_re;
//	complex<double> exp_x_sq_0_p_ae;
//	complex<double> exp_x_sq_0_m_ae;
//	complex<double> exp_x_sq_1_p_ae;
//	complex<double> exp_x_sq_1_m_ae;

	//transforming x and x² to Spin-Boson Hilbert space

	x_SB = kroneckerProduct(H_S_I, x);
	x_sq_SB = kroneckerProduct(H_S_I, x_sq);

	//calculating analytic and numeric version von <x>

	exp_x_0_m_a = 2 * norm_0_sqr * (eta_m / omega);
	exp_x_0_p_a = 2 * norm_1_sqr * (eta_p / omega);
//	exp_x_0_p_a = 2 * tmp_p.real() / norm_p / norm_p;
//	exp_x_0_m_a = 2 * tmp_m.real() / norm_m / norm_m;
//	exp_x_1_p_a = - 2 * tmp_p.real();
//	exp_x_1_m_a = -2 * tmp_m.real();
//changed
	exp_x_0_p_n = expVF(d_H_SB_SB.eigenvectors().col(0), x_SB);
	exp_x_0_m_n = expVF(d_H_SB_SB.eigenvectors().col(1), x_SB);
//	exp_x_1_p_n = expVF(d_H_SB_SB.eigenvectors().col(2), x_SB);
//	exp_x_1_m_n = expVF(d_H_SB_SB.eigenvectors().col(3), x_SB);

//	cout << "<x_0_+>_a = " << exp_x_0_p_a << endl;
//	cout << "<x_1_+>_a = " << exp_x_1_p_a << endl;
//	cout << "<x_0_->_a = " << exp_x_0_m_a << endl;
//	cout << "<x_1_->_a = " << exp_x_1_m_a << endl;
//
//	cout << "numerisch" << endl;
//	cout << "<x_0_+>_n = " << exp_x_0_p_n << endl;
//	cout << "<x_1_+>_n = " << exp_x_1_p_n << endl;
//	cout << "<x_0_->_n = " << exp_x_0_m_n << endl;
//	cout << "<x_1_->_n = " << exp_x_1_m_n << endl;

	//calculating errors for <x>

//	exp_x_0_p_ae = abs(exp_x_0_p_a - exp_x_0_p_n);
//	exp_x_1_p_ae = abs(exp_x_1_p_a - exp_x_1_p_n);
//	exp_x_0_m_ae = abs(exp_x_0_m_a - exp_x_0_m_n);
//	exp_x_1_m_ae = abs(exp_x_1_m_a - exp_x_1_m_n);

//	exp_x_0_p_re = abs(exp_x_0_p_a - exp_x_0_p_n) / abs(exp_x_0_p_a);
//	exp_x_1_p_re = abs(exp_x_1_p_a - exp_x_1_p_n) / abs(exp_x_1_p_a);
//	exp_x_0_m_re = abs(exp_x_0_m_a - exp_x_0_m_n) / abs(exp_x_0_m_a);
//	exp_x_1_m_re = abs(exp_x_1_m_a - exp_x_1_m_n) / abs(exp_x_1_m_a);

//	cout << "absoluter Fehler" << endl;
//	cout << "Δ<x_0_+> = " << exp_x_0_p_ae << endl;
//	cout << "Δ<x_1_+> = " << exp_x_1_p_ae << endl;
//	cout << "Δ<x_0_-> = " << exp_x_0_m_ae << endl;
//	cout << "Δ<x_1_-> = " << exp_x_1_m_ae << endl;
//
//	cout << "relative Fehler" << endl;
//	cout << "Δ<x_0_+> = " << exp_x_0_p_re << endl;
//	cout << "Δ<x_1_+> = " << exp_x_1_p_re << endl;
//	cout << "Δ<x_0_-> = " << exp_x_0_m_re << endl;
//	cout << "Δ<x_1_-> = " << exp_x_1_m_re << endl;

	//calculating analytic and numeric version of <x²>

	exp_x_sq_0_p_a = norm_0_sqr * (1 + 3 * eta_m.real() * eta_m.real() / omega / omega  + 3 * zeta.real() * zeta.real() / tmp_0.real() / tmp_0.real());
//	exp_x_sq_0_p_a = (1 + 3 * tmp_p.real() * tmp_p.real()) / norm_p / norm_p;
//	exp_x_sq_1_p_a = 3 + tmp_p.real() * tmp_p.real();
	exp_x_sq_0_m_a = norm_1_sqr * (1 + 3 * eta_p.real() * eta_p.real() / omega / omega + 3 * zeta.real() * zeta.real() / tmp_1.real() / tmp_1.real());
//	exp_x_sq_0_m_a = 1 + 3 * tmp_m.real() * tmp_m.real() / norm_m / norm_m;
//	exp_x_sq_1_m_a = 3 + tmp_m.real() * tmp_m.real();

	exp_x_sq_0_p_n = expVF(d_H_SB_SB.eigenvectors().col(0), x_sq_SB);
	exp_x_sq_0_m_n = expVF(d_H_SB_SB.eigenvectors().col(1), x_sq_SB);
//	exp_x_sq_1_p_n = expVF(d_H_SB_SB.eigenvectors().col(2), x_sq_SB);
//	exp_x_sq_1_m_n = expVF(d_H_SB_SB.eigenvectors().col(3), x_sq_SB);

//	cout << endl << "Erwartungswert x²" << endl;
//	cout << "analytisch" << endl;
//	cout << "<x_0^2_+>_a = " << exp_x_sq_0_p_a << endl;
//	cout << "<x_1^2_+>_a = " << exp_x_sq_1_p_a << endl;
//	cout << "<x_0^2_->_a = " << exp_x_sq_0_m_a << endl;
//	cout << "<x_1^2_->_a = " << exp_x_sq_1_m_a << endl;
//
//	cout << "numerisch" << endl;
//	cout << "<x_0^2_+>_n = " << exp_x_sq_0_p_n << endl;
//	cout << "<x_1^2_+>_n = " << exp_x_sq_1_p_n << endl;
//	cout << "<x_0^2_->_n = " << exp_x_sq_0_m_n << endl;
//	cout << "<x_1^2_->_n = " << exp_x_sq_1_m_n << endl;

////calculating errors for <x²>

//	exp_x_sq_0_p_ae = abs(exp_x_sq_0_p_a - exp_x_sq_0_p_n);
//	exp_x_sq_1_p_ae = abs(exp_x_sq_1_p_a - exp_x_sq_1_p_n);
//	exp_x_sq_0_m_ae = abs(exp_x_sq_0_m_a - exp_x_sq_0_m_n);
//	exp_x_sq_1_m_ae = abs(exp_x_sq_1_m_a - exp_x_sq_1_m_n);

//	exp_x_sq_0_p_re = abs(exp_x_sq_0_p_a - exp_x_sq_0_p_n) / abs(exp_x_sq_0_p_a);
//	exp_x_sq_1_p_re = abs(exp_x_sq_1_p_a - exp_x_sq_1_p_n) / abs(exp_x_sq_1_p_a);
//	exp_x_sq_0_m_re = abs(exp_x_sq_0_m_a - exp_x_sq_0_m_n) / abs(exp_x_sq_0_m_a);
//	exp_x_sq_1_m_re = abs(exp_x_sq_1_m_a - exp_x_sq_1_m_n) / abs(exp_x_sq_1_m_a);

////cout << "absoluter Fehler" << endl;
////cout << "Δ<x²_0_+> = " << exp_x_sq_0_p_ae << endl;
////cout << "Δ<x²_1_+> = " << exp_x_sq_1_p_ae << endl;
////cout << "Δ<x²_0_-> = " << exp_x_sq_0_m_ae << endl;
////cout << "Δ<x²_1_-> = " << exp_x_sq_1_m_ae << endl;
////
////cout << "relative Fehler" << endl;
////cout << "Δ<x²_0_+> = " << exp_x_sq_0_p_re << endl;
////cout << "Δ<x²_1_+> = " << exp_x_sq_1_p_re << endl;
////cout << "Δ<x²_0_-> = " << exp_x_sq_0_m_re << endl;
////cout << "Δ<x²_1_-> = " << exp_x_sq_1_m_re << endl;

///*
// * write to file
// */
	FILE *file = fopen(filename.c_str(), "a");
	fprintf(file, "%1s %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f\n", " ", lambda, sbE_a[0].real(), sbE_n[0].real(), sbE_a[2].real(), sbE_n[2].real(), exp_x_0_p_a.real(), exp_x_0_p_n.real(), exp_x_0_m_a.real(), exp_x_0_m_n.real(), exp_x_sq_0_p_a.real(), exp_x_sq_0_p_n.real(), exp_x_sq_0_m_a.real(), exp_x_sq_0_m_n.real());
	fclose(file);

}

int main()
{
	Eigen::initParallel();

	using std::string;
	using std::to_string;
	using std::cout;
	using std::endl;

	// parameter scan in λ for Δ = 0.1, ε = 0.1, ω = 1, N = {2, 3, 4, 5, 100}

	size_t N[] = {2, 3, 4, 5, 100};
	double epsilon = 0.1;
	double delta = 0.1;
	double omega = 1;
	double lambda_min = 0;
	double lambda_max = 2.0;
	unsigned int repetition = 1000;

	double h = (lambda_max - lambda_min) / (double) repetition;

	for(auto j : N)
	{
		std::string filename = ("build/data/pm-scan_in_λ_ω=20_ε=Δ=2_N=" + to_string(j) + ".txt");
		FILE *file = fopen(filename.c_str(), "w");
		fprintf(file, "%1s %.15s %.15s %.15s %.15s %.15s %.15s %.15s %.15s %.15s %.15s %.15s %.15s %.15s\n", "#", "λ", "E_p_a", "E_p_n", "E_m_a", "E_m_n", "<x_0_a>+", "<x_0_n>+", "<x_0_a>-", "<x_0_n>-", "<x²_0_a>+", "<x²_0_n>+", "<x²_0_a>-", "<x²_0_n>-");
		fclose(file);

		for (unsigned int i = 0; i <= repetition; i++)
		{
			cout << "Fock States coupling variation: N = " << j << " | " << i << " / " << repetition << " iterations" << endl;
			unshiftedFockStates(delta, epsilon, i * h, omega, j, filename);
		}
	}
	return 0;
}
