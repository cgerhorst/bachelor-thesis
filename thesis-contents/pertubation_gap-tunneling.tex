Für die Störungsrechnung wird die 1. Wilson Schale \eqref{eqn:hamilton_first-wilson}
\begin{eqn}
  \mathcal{H} = \underbrace{\rule[-0.515\baselineskip]{0pt}{\baselineskip} ω_0 \Dagger{\bar{B}_0} \bar{B}_0^{} - E_\t{G}}_\mathcal{H_0} \underbrace{- \frac{Δ}{2} σ_x^{}}_{\mathcal{V}_1} + \underbrace{\frac{ε}{2} σ_z^{} }_{\mathcal{V}_2} \eqlabel{eqn:strong-coupling-hamiltonian}
\end{eqn}
mit $E_{\t{G}} = \bar{λ}^2 / 4 ω_0$, dem ungestörten Hamiltonoperator $\mathcal{H}_0$ und den Störtermen $\mathcal{V}_1$ und $\mathcal{V}_2$ betrachtet.
Dabei ergibt sich der ungestörte Teil $\mathcal{H}_0$ aus einer zu \eqref{eqn:displaced_operators} analogen Rechnung, die Wirkung der Leiteroperatoren ist auch analog zu \eqref{eqn:babar} und \eqref{eqn:bdaggerbar}.

Es muss gefordert werden, dass
\begin{eqn}
  \frac{ε}{ω_0} \ll 1 \quad \t{und} \quad \frac{Δ}{ω_0} \ll 1 \eqlabel{eqn:cond_Δ-pertubaton}
\end{eqn}
sind.
Des Weiteren werden nun lediglich die entarteten Grundzustände
\begin{eqn}
  \g\{\ket{↑, θ^{↑}}; \ket{↓, θ^{↓}}\} \eqlabel{eqn:eigenbasis_pertubation-strong}
\end{eqn}
als normierte Eigenbasis verwendet, wobei $\ket{θ^{↑}} = \ket{θ}$ und $\ket{θ^{↓}} = \ket{-θ}$ die kohärenten Zustände der nach rechts und links verschobenen harmonischen Oszillatoren sind.
Um die Wirkung von $\mathcal{V}_1$ auf die Eigenzustände zu erhalten, wird $σ_x$ durch die Spinleiteroperatoren $σ_{+}$ und $σ_{-}$ ausgedrückt:
\begin{eqn}
  σ_x = \frac{1}{2} \g(σ_{+} + σ_{-})\pns.
\end{eqn}
Für sie gilt
\begin{eqns}
  σ_{±} \ket{↑\!\!/\!\!↓, θ^{↑/↓}} &=& 0 \pns,\IEEEyesnumber \IEEEyessubnumber*\\
  σ_{±} \ket{↓\!\!/\!\!↑, θ^{↓/↑}} &=& 2 \ket{↑\!\!/\!\!↓, θ^{↓/↑}}  \pns.
\end{eqns}

Für die entartete Störungsrechnung (vergleiche \cite[Kap. 12.2]{physik4_skript}) wird ein Projektor
\begin{eqn}
  P_n = \sum_{r=1}^{g} \ket{E_n^0, r}\bra{E_n^0, r}\pns, \quad \sum_n P_n = \idm
\end{eqn}
definiert, der den Hamiltonoperator \eqref{eqn:strong-coupling-hamiltonian} in den $g$-fach entarteten Unterraum der $n$-ten exakten Energie $E_n^0$ von $\mathcal{H}_0$ projiziert und die Vollständigkeitsrelation erfüllt.
Die Matrixelemente des Hamiltonoperators \eqref{eqn:strong-coupling-hamiltonian} ergeben sich durch
\begin{eqn}
  \mathcal{H} = \idm \mathcal{H} \idm = \sum_n \sum_m P_n \mathcal{H} P_m = \sum_n P_n \mathcal{H} P_n + \sum_{n \neq m} P_n \mathcal{H} P_m\pns. \IEEEyesnumber \IEEEyessubnumber \eqlabel{eqn:hamiltonian-projection}
\end{eqn}
Da die Eigenbasis \eqref{eqn:eigenbasis_pertubation-strong} hier nur Grundzustände enthält, vereinfacht sich \eqref{eqn:hamiltonian-projection} zu
\begin{eqn}
  \mathcal{H} \approx P_0 \g(\mathcal{H}_0 + \mathcal{V}_1 + \mathcal{V}_2) P_0\pns, \IEEEyessubnumber \eqlabel{eqn:simple_hamiltonian-projection}
\end{eqn}
mit
\begin{eqn}
  P_0 =  \ket{\ucu} \bra{\ucu} + \ket{\dcd} \bra{\dcd} \IEEEyessubnumber\pns.
\end{eqn}
Nach Anwenden des Projektors ergibt sich die $2 {\times} 2$-Matrix
\begin{eqns}
  \mathcal{H}_{\g(s, θ), \g(s', θ')} = \braket{\rule{0pt}{1em}s, θ}[\g(\mathcal{H}_0 + \mathcal{V}_1 + \mathcal{V}_2)]{s', θ'} = E_0 \Kroneckerdelta{_s_{s'}} + \braket{s, θ}[\g(\mathcal{V}_1 + \mathcal{V}_2)]{s', θ'} \pns. \IEEEeqnarraynumspace \IEEEyessubnumber
\end{eqns}
Wegen der Entartung sind beide Eigenwerte $E_0$ von $\mathcal{H}^0$ gleich und lauten
\begin{eqn}
  E_0 = \braket{↑\!\!/\!\!↓, θ^{↑/↓}}[{\mathcal{H}_0}]{↑\!\!/\!\!↓, θ^{↑↓}} = \braket{\rule{0pt}{1.25em}↑\!\!/\!\!↓, θ^{↑↓}}[\g(ω_0 \Dagger{\bar{B}_0} \bar{B}_0^{} - E_\t{G}^{})]{↑\!\!/\!\!↓, θ^{↑↓}} = -E_\t{G}^{} \pns.\IEEEeqnarraynumspace
\end{eqn}
Für die Matrixelemente der Störung gilt mit $\mathcal{V} = \mathcal{V}_1 + \mathcal{V}_2$
\begin{eqns}
  \braket{\ucu}[\mathcal{V}]{\ucu} &=& -\frac{Δ}{2} \braket{\ucu}{\dcu} + \frac{ε}{2} \braket{\ucu}{\ucu} = \frac{ε}{2} \IEEEeqnarraynumspace \IEEEeqnarraynumspace \IEEEeqnarraynumspace \IEEEeqnarraynumspace\\
  \braket{\dcd}[\mathcal{V}]{\dcd} &=& -\frac{Δ}{2} \braket{\dcd}{\ucd} - \frac{ε}{2} \braket{\dcd}{\dcd} = -\frac{ε}{2} \\
  \braket{\ucu}[\mathcal{V}]{\dcd} &=& -\frac{Δ}{2} \braket{\ucu}{\ucd} - \frac{ε}{2} \braket{\ucu}{\dcd} \IEEEyesnumber \IEEEyessubnumber \\
                                   &=& -\frac{Δ}{2} \exp(-\frac{1}{2}(θ^2 + θ^2) - θ^2) = -\frac{Δ}{2} \exp(-2 θ²) = -\frac{\tilde{Δ}}{2} \IEEEyessubnumber\\
  \braket{\dcd}[\mathcal{V}]{\ucu} &=& -\frac{Δ}{2} \braket{\ucu}{\ucd} + \frac{ε}{2} \braket{\dcd}{\ucu} \IEEEyesnumber \IEEEyessubnumber \\
                                 &=& -\frac{Δ}{2} \exp(-\frac{1}{2}(θ^2 + θ^2) - θ^2) = -\frac{Δ}{2} \exp(-2 θ²) = -\frac{\tilde{Δ}}{2}\pns.\IEEEyessubnumber
\end{eqns}
Die Eigenwerte der sich ergebenden Matrix
\begin{eqn}
  \mathcal{H} =  \begin{pmatrix}
                          -E_\t{G} + \frac{ε}{2} & -\frac{\tilde{Δ}}{2} \\
                          -\frac{\tilde{Δ}}{2} & -E_\t{G} - \frac{ε}{2}
                         \end{pmatrix} \eqlabel{eqn:full-pertubation-matrix}
\end{eqn}
sind die korrigierten Energien 1. Ordnung und lauten
\begin{eqn}
  E^1_{0,1} = -E_\t{G} \mp \frac{\sqrt{ε^2 + \tilde{Δ}^2}}{2}\pns. \eqlabel{eqn:eigenenergies_strong}
\end{eqn}

Diese Eigenenergien sind nicht mehr entartet, sondern spalten sich auf und stoßen sich ab.
Die Energiekorrektur, welche die Entartung aufhebt, beinhaltet ein renormiertes $\tilde{Δ}$, das mit größerer Verschiebung mit der Ordnung $\LandauO(\cramped{\E^{-θ^2}})$ abnimmt.
Wegen des quadratischen Argumentes in der Exponentialfunktion ist dieser Effekt unabhängig von der Richtung der Verschiebung.
Der Grund für die Renormierung ist die Kopplung an den harmonischen Oszillator, denn falls $\bar{λ} = 0$ ist, folgt $\tilde{Δ} ~= Δ$.

Die orthonormierten Eigenzustände ergeben sich aus den Eigenvektoren von~\eqref{eqn:full-pertubation-matrix} und lauten
\begin{eqns}
  \ket{Ψ_{0, 1}} &=& \frac{\tilde{Δ}}{\sqrt{\g(ε \pm \sqrt{\tilde{Δ}^2 + ε^2})^{\!\!2} + \tilde{Δ}^2}} \ket{\ucu} + \frac{- ε \mp \sqrt{\tilde{Δ}^2 + ε^2}}{\sqrt{\g(ε \pm \sqrt{\tilde{Δ}^2 + ε^2})^{\!\!2} + \tilde{Δ}^2}} \ket{\dcd} \IEEEeqnarraynumspace \IEEEyesnumber \IEEEyessubnumber* \eqlabel{eqn:strong_eigenzustand}\\
              &=& α_{∓} \ket{\ucu} + β_{∓} \ket{\dcd} \pns.
\end{eqns}

Der Tunnelterm führt zu einer Delokalisierung der harmonischen Oszillatoren, die Zustände mischen also.
Dadurch geht die Eindeutigkeit der Spinausrichtung verloren; $σ_z$ ist also keine Erhaltungsgröße mehr.
Des Weiteren sind die Zustände formgleich zu bindenden und antibindenden Zuständen, tragen jedoch eine unterschiedliche Gewichtung $α_{∓}$ und $β_{∓}$.

Die Störung entspricht einem Magnetfeld $\Transpose{\v{B}} = \begin{pmatrix}
                                                               \tilde{Δ}, 0, ε
                                                            \end{pmatrix}$, welches den lokalen Spin in seine Richtung polarisiert und die Entartung aufhebt.
Für unendlich starke Kopplung gilt $\tilde{Δ} \rightarrow 0$.
Das System wird somit in $z$-Richtung ausgerichtet und die harmonischen Oszillatoren werden wieder lokalisiert, da es kein Tunneln mehr gibt.
Der Energieabstand
\begin{eqn}
  d = \sqrt{ε^2 + \tilde{Δ}^2}
\end{eqn}
ist die effektive Länge des Magnetfeldvektors $\v{B}$.
