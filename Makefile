DATA_COUPLING := N=2 N=3 N=4 N=5 N=100
DATA_COUPLING := $(addprefix build/data/pm-scan_in_λ_ω=20_ε=Δ=2_,$(addsuffix .txt,$(DATA_COUPLING)))

DATA_TUNNELING := N=3 N=4 N=5 N=100
DATA_TUNNELING := $(addprefix build/data/pm-scan_in_Δ_ω=20_ε=0_λ=15_,$(addsuffix .txt,$(DATA_TUNNELING)))

DATA_THETA := build/data/coherent-states_pm-scan_in_θ_max_Δ=3_ω=20_ε=0_λ=100_N=11.txt

DATA_COHERENT := N=3 N=5 N=7 N=11
DATA_COHERENT := $(addprefix build/data/coherent-states_pm-scan_in_Δ_ω=20_ε=0_λ=100_θmax=5_,$(addsuffix .txt,$(DATA_COHERENT)))

DATA_EXPECTATION := build/data/pm-scan_in_Δ_ω=20_ε=0_λ=15_N=5.txt

DATA_COMPAREFOCK := build/data/pm-scan_in_Δ_ω=20_ε=0_λ=100_N=24.txt

FIGURESTHESIS := $(addprefix λ/,$(addsuffix _thesis.pdf,E expX expX2)) \
		   $(addprefix Δ/,$(addsuffix _thesis.pdf,E expX2)) \
		   $(addprefix coherent/,$(addsuffix _thesis.pdf,minimum E)) \
		   expectation/expX2_thesis.pdf
FIGURESTHESIS := $(addprefix build/graphic/,$(FIGURESTHESIS))

FIGURESTALK := $(addprefix λ/,$(addsuffix _talk.pdf,E expX expX2)) \
		   $(addprefix Δ/,$(addsuffix _talk.pdf,E expX2)) \
		   $(addprefix coherent/,$(addsuffix _talk.pdf,minimum E)) \
		   expectation/expX2_talk.pdf
FIGURESTALK := $(addprefix build/graphic/,$(FIGURESTALK))

THESISCONTENTS := titlepage abstract \
			spin-boson-hamiltonian nrg outline interpretation_spin-boson\
			strong_exact-solution first_wilson pertubation_gap-tunneling pertubation_tunneling expectation_strong\
			weak_exact-solution pertubation_coupling expectation_weak\
			numerics_preliminary_remark technical_approach coupling_variation tunneling_variation expectation_comparison coherent_comparison \
			result-outlook \
			acknowledgement oath_admonition
THESISCONTENTS := $(addprefix thesis-contents/,$(addsuffix .tex,$(THESISCONTENTS)))

TALKCONTENTS := cover \
		spin-boson-model interpretation_spin-boson nrg motivation \
		base chain-nrg exact nrg\
		strong_analytic strong_numeric \
		weak_analytic weak_numeric \
		result \
		pertubation numerics bathspectral otherGraph graphs
TALKCONTENTS := $(addprefix talk/,$(addsuffix .tex,$(CONTENTSTALK)))


TEXENV := TEXINPUTS="$(shell pwd)/build/:$(shell pwd)/:$(shell pwd)/header-components/:" max_print_line=1048576
TEXARG := --shell-escape --halt-on-error --interaction=batchmode --output-directory=build
TEX := $(TEXENV) lualatex $(TEXARG)

all: build/thesis_print.pdf build/thesis_electronic.pdf build/talk.pdf build/talk_handout.pdf

include code/Makefile script/Makefile

build/thesis_print.pdf: $(FIGURESTHESIS) $(THESISCONTENTS) thesis.tex header/thesis_header.tex header/thesis_packages.tex literature.bib common.bib
	@./tex.sh thesis.tex thesis_print

build/thesis_electronic.pdf: $(FIGURESTHESIS) $(THESISCONTENTS) thesis.tex header/thesis_header.tex header/thesis_packages.tex literature.bib common.bib
	@./tex.sh thesis.tex thesis_electronic

build/talk_handout.pdf: $(FIGURESTALK) talk.tex header/talk_header.tex header/talk_packages.tex $(TALKCONTENTS) literature.bib common.bib| build
	@./tex.sh talk.tex talk_handout

build/talk.pdf: $(FIGURESTALK) talk.tex header/talk_header.tex header/talk_packages.tex $(TALKCONTENTS) literature.bib common.bib| build
	@./tex.sh talk.tex talk

code: build/code/fock_weak build/code/fock_strong build/code/coherent

data: $(DATA_COHERENT) $(DATA_COUPLING) $(DATA_TUNNELING) $(DATA_THETA) $(DATA_EXPECTATION) $(DATA_COMPAREFOCK)

figures: $(FIGURESTHESIS) $(FIGURESTALK)

clean:
	 rm -rf build

fte:
	@./tex.sh --fast thesis.tex thesis_electronic

ftp:
	@./tex.sh --fast thesis.tex thesis_print

fto:
	@./tex.sh --fast talk.tex talk

fth:
	@./tex.sh --fast talk.tex talk_handout

lit:
	@echo biber --logfile build/main.blg --outfile build/main.bbl build/main.bcf
	@biber --logfile build/main.blg --outfile build/main.bbl build/main.bcf | grep -E "WARN|ERROR" ;\
	if ! [ $${PIPESTATUS[0]} -eq 0 ] ; then \
		rm -f main.pdf ;\
		exit 1 ;\
	fi

.PHONY: clean fte ftp fto fth data code lit figures
