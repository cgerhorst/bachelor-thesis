Ein unendlich dimensionaler Hilbertraum muss in der Numerik trunkiert werden.
Der Hamiltonoperator und andere Operatoren werden dann durch Matrizen repräsentiert, deren exakte Diagonalisierung \cite{eigenweb} zu den numerischen Eigenwerten und den zugehörigen Eigenzuständen führt.

Bei diesem Modell ist jeder Term von $\mathcal{H}$ und den Operatoren $x$ und $x²$ ein Kronecker-Produkt $S \otimes B$ aus Spinanteil $S$ und bosonischem \mbox{Anteil $B$}. Wenn ein Teil fehlt, wird die jeweilige Identität eingesetzt.
Für die Berechnung von $\avg{x \vphantom{^2}}_n$ und $\avg{x^2}_n$ wird der $n$-te Eigenzustand benutzt.
\paragraph{Fockbasis}
  Bei der Fockbasis lauten die Matrixelemente der bosonischen Leiteroperatoren
  \begin{IEEEeqnarray}{c'u"c'u}
    \IEEEyesnumber \IEEEyessubnumber \IEEEnonumber
    \begin{IEEEeqnarraybox}{c}
      b_{n', n}^{} = \braket{n'}[b]{n} = \sqrt{n} \Kroneckerdelta{_{n',}_{n-1}}
    \end{IEEEeqnarraybox} & \theIEEEsubequationdis &
    \stepcounter{IEEEsubequation}
    \begin{IEEEeqnarraybox}{c}
      \Dagger{b}_{n', n} = \braket{n'}[\Dagger{b}]{n} = \sqrt{n + 1} \Kroneckerdelta{_{n',}_{n+1}}\pns,
    \end{IEEEeqnarraybox} & \theIEEEsubequationdis \IEEEnonumber
  \end{IEEEeqnarray}
  mit den Fockzuständen $\ket{n}$.
  Die bosonischen Matrizen sind von der Dimension $N {\times} N$.
\paragraph{Kohärente Basis}
  Die kohärente Basis besteht aus $2N + 1$ kohärenten Zuständen $\ket{lθ}$, \mbox{$l \in \g\{-N, \ldots, 0, \ldots, N\}$}, die äquidistant im Intervall $\intcc{-θ_\t{max}}{θ_\t{max}}$ verteilt sind.
  Somit ergibt sich
  \begin{eqn}
    θ = \frac{θ_\t{max}}{N}\pns.
  \end{eqn}

  Wegen \eqref{eqn:overlap} muss die kohärente Basis orthonormalisiert werden. Dazu wird eine Skalarproduktmatrix $\m{S}$ mit den Elementen
  \begin{eqn}
    S_{k, l} = \braket{k θ}{l θ} = \exp(- \frac{θ^2}{2} \g(k - l)²) \eqlabel{eqn:numeric_overlap}
  \end{eqn}
  aufgebaut, deren Spalten die Koeffizienten der Basisvektoren $\g\{\g(\ket{l θ})\}$ enthalten.
  Diese Matrix wird mit dem Gram-Schmidt-Verfahren orthonormiert, sodass die neue $n$-te Spalte die normierten Gewichte~$a_l^{\overline{n θ}}$ der Linearkombination
  \begin{eqn}
    \ket{\overline{n θ}} = \sum_{l = -N}^{N} a_{l}^{\overline{n θ}} \ket{l θ}
  \end{eqn}
  aus den ursprünglichen Basisvektoren $\ket{l θ}$ beinhaltet; $\ket{\overline{nθ}}$ ist dann der orthonormierte kohärenter Zustand.
  Für $θ \rightarrow 0$ treten Probleme bei der Orthonormierung auf, da die Koeffizienten $a_l^{\overline{n θ}}$ divergieren.

  Mit \eqref{eqn:numeric_overlap} und \eqref{eqn:eigenvalue_annihilation} ergeben sich die Matrixelemente der bosonischen Leiteroperatoren
  \begin{eqns}
    b_{m, n} &=& \braket{\overline{m θ}}[b]{\overline{n θ}} = \sum_{k, l} \conj{\g(a_k^{\overline{m θ}})} a_l^{\overline{n θ}} \braket{k}{b l} = \sum_{k, l} \conj{\g(a_k^{\overline{m θ}})} a_l^{\overline{n θ}} l S_{k, l} \IEEEyesnumber \IEEEyessubnumber*\\
    \Dagger{b}_{m, n} &=& \braket{\overline{m θ}}[\Dagger{b}]{\overline{n θ}} = \sum_{k, l} \conj{\g(a_k^{\overline{m θ}})} a_l^{\overline{n θ}} \braket{b k}{l} = \sum_{k, l} \conj{\g(a_k^{\overline{m θ}})} a_l^{\overline{n θ}} \conj{k} S_{k, l}
  \itext{und des Besetzungszahloperators}
  \g(\Dagger{b}b^{})_{m, n} &=& \braket{\overline{m θ}}[\Dagger{b} b]{\overline{n θ}} = \sum_{k, l} \conj{\g(a_k^{\overline{m θ}})} a_l^{\overline{nθ}} \braket{b k}{b l} = \sum_{k, l} \conj{\g(a_k^{\overline{mθ}})} a_l^{\overline{n θ}} \conj{k} l S_{k, l}\pns. \IEEEeqnarraynumspace \eqlabel{eqn:coherent_number-operator}
  \end{eqns}
  Für $k = l$ folgt aus \eqref{eqn:coherent_number-operator}, dass die Diagonalelemente quadratisch, also nicht äquidistant sind; durch die Nebendiagonalelemente kommt eine Korrektur hinzu.
