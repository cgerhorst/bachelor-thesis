import numpy as np
import matplotlib.pyplot as plt

Δ, exp_1_p,  exp_1_m, exp_2_p, exp_2_m, exp_3_p, exp_3_m, exp_4_p, exp_4_m = np.loadtxt("build/data/pm-scan_in_Δ_ω=20_ε=0_λ=15_N=5.txt", skiprows=1, usecols=(0, 6, 8, 9, 10, 11, 12, 13, 14), unpack=True)

fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax1.plot(Δ, exp_1_p - 1 - 0.75**2, 'b-', linewidth=1.9, label=r'$\avg{x^2}_0 \!\!\! - \avg{x^2}_0^{\t{HO}}$')
ax1.plot(Δ, exp_1_m - 1 - 0.75**2, 'g-', linewidth=1.9, label=r'$\avg{x^2}_1 \!\!\! - \avg{x^2}_0^{\t{HO}}$')
ax1.plot(Δ, exp_2_p - 3 - 0.75**2, 'r-', linewidth=1.9, label=r'$\avg{x^2}_2 \!\!\! - \avg{x^2}_1^{\t{HO}}$')
ax1.plot(Δ, exp_2_m - 3 - 0.75**2, 'c-', linewidth=1.9, label=r'$\avg{x^2}_3 \!\!\! - \avg{x^2}_1^{\t{HO}}$')
ax1.plot(Δ, exp_3_p - 5 - 0.75**2, '-', linewidth=1.9, color='#FF9900', label=r'$\avg{x^2}_4 \!\!\! - \avg{x^2}_2^{\t{HO}}$')
ax1.plot(Δ, exp_3_m - 5 - 0.75**2, 'm-', linewidth=1.9, label=r'$\avg{x^2}_5 \!\!\! - \avg{x^2}_2^{\t{HO}}$')
ax1.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3, ncol=3, mode="expand", borderaxespad=0.)
ax1.set_ylim(-1.0, 0.5)
ax1.set_xlim(0, 0.302)
ax1.set_xlabel(r'$Δ / ω_0$')
ax1.set_ylabel(r'$\avg{x^2}_n - \avg{x^2}_m^{\t{HO}} - \frac{\bar{λ}^2}{ω_0^2}$')
fig.set_tight_layout(False)
fig.savefig('build/graphic/expectation/expX2_talk.pdf')
