#include <complex>
#include <iostream>
#include <cmath>
#include <string>
#include <cstdio>

//linear algebra with Eigen
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/KroneckerProduct>

//paralellisation with omp
#include <omp.h>

//paralellisation paramter for omp
//#define CHUNKSIZE 1

//Expected value with state l and operator op
#define expVF(l, op) (l.adjoint() * (op * l))(0,0)

using Eigen::Vector2cd;
using Eigen::Matrix2cd;
using Eigen::MatrixXcd;
using std::complex;

// MatrixS = spin matrices
typedef Matrix2cd MatrixS;

MatrixS sigmaX;
MatrixS sigmaZ;

/*
 * data structure for cohStates consisting of displacement sigma, base matrix, scalar product matrix, ladder operator b and number operator n
 */
struct cohStates
{
	double sigma;
	MatrixXcd base;
	MatrixXcd spm;
	MatrixXcd b;
	MatrixXcd n;
};

/*
 * initialize Pauli matrices
 */
void createPauli()
{
	sigmaX << 0, 1,
			  1, 0;

	sigmaZ << 1,  0,
			  0, -1;
}

/*
 * creating bosonic ladder operators (either daggered or not) for oModes states
 */
MatrixXcd createLadderOpF(bool dagger, MatrixXcd::Index oModes)
{
	MatrixXcd b(oModes, oModes);
	MatrixXcd::Index i, j;

	for (i = 0; i < b.rows(); i++)
	{
		for(j = 0; j < b.cols(); j++)
		{
			b(i, j) = (i == j - 1) ? sqrt(j): 0;
		}
	}

	if (dagger)
	{
		b.adjointInPlace();
	}

	return b;
}

/*
 * get individual displacement of the i-th coherent state for given single displacement "displ" (left displaced oscillators are on even, right displaced oscillators are on odd places in the matrix)
 */
double getStateValue(MatrixXcd::Index i, double displ)
{
	if (i > 0 && i % 2 == 1)
	{
		return double(i / 2 + 1) * displ;
	}
	if (i > 0 && i % 2 == 0)
	{
		return double(- i / 2) * displ;
	}

	return 0;
}

/*
 * get overlap of the m-th and n-th coherent state for given scalar product matrix and base matrix
 */
complex<double> overlapfactor(MatrixXcd spm, MatrixXcd base, MatrixXcd::Index m, MatrixXcd::Index n)
{
	complex<double> result = 0;
	for (MatrixXcd::Index k = n; k >= 0; k--)
	{
		result += spm(m, k) * base(k, n);
	}
	return result;
}


/*
 * create coherent base of oModes states with a given single displacement
 */
cohStates createCohBase(MatrixXcd::Index oModes, double displacement)
{
	cohStates c;
	c.sigma = displacement;

	//create scalar product matrix
	c.spm = MatrixXcd::Zero(oModes, oModes);

	for (MatrixXcd::Index i = 0; i < oModes; i++)
	{
		for(MatrixXcd::Index j = 0; j < oModes; j++)
		{
			c.spm(i, j) = exp(-0.5 * (getStateValue(i, c.sigma) - getStateValue(j, c.sigma)) * (getStateValue(i, c.sigma) - getStateValue(j, c.sigma)));
		}
	}

	//create BaseMatrix
	c.base = MatrixXcd::Identity(oModes, oModes);

	//i and j run through upper diagonal matrix
	for(MatrixXcd::Index j = 1; j < oModes; j++)
	{
		for(MatrixXcd::Index i = j - 1; i >= 0; i--)
		{
			for(MatrixXcd::Index k = i; k >=0; k--)
			{
				c.base(k, j) -= overlapfactor(c.spm, c.base, j, i) * c.base(k, i);
			}
		}

		//normalize states
		complex<double> norm = 0;

		for(MatrixXcd::Index l = 0; l < oModes; l++)
		{
			for(MatrixXcd::Index m = 0; m < oModes; m++)
			{
				norm += c.base(l, j) * c.base(m, j) * c.spm(l, m);
			}
		}
		for(MatrixXcd::Index n = 0; n < oModes; n++)
			c.base(n, j) /= sqrt(norm);
	}

	//create number operator and ladder operator
	c.n = MatrixXcd::Zero(oModes, oModes);
	c.b = MatrixXcd::Zero(oModes, oModes);

	for(MatrixXcd::Index i = 0; i < oModes; i++)
	{
		for(MatrixXcd::Index j = 0; j < oModes; j++)
		{
			for(MatrixXcd::Index k = 0; k < oModes; k++)
			{
				for(MatrixXcd::Index l = 0; l < oModes; l++)
				{
					c.b(i, j) += c.base(k, i) * c.base(l, j) * getStateValue(l, c.sigma) * c.spm(k, l);
					c.n(i, j) += c.base(k, i) * c.base(l, j) * getStateValue(k, c.sigma) * getStateValue(l, c.sigma) * c.spm(k, l);
				}
			}
		}
	}
	return c;
}

/*
 * build Hamiltonian matrix for spin-space for given bias epsilon and tunneling amplitude delta
 */
MatrixS buildHamiltonianS(double epsilon, double delta)
{
	createPauli();
	return - delta / 2 * sigmaX + epsilon / 2 * sigmaZ;
}

/*
 * build Hamiltonian matrix for bosonic-space for given frequency omega, number of states oModes and coherent states data structure(for number operator)
 */
MatrixXcd buildHamiltonianB(double omega, MatrixXcd::Index oModes, cohStates c)
{
	MatrixXcd n = c.n;

	return omega * c.n;
}

/*
 * build Hamiltonian matrix for coupling term for given coupling lambda, oModes states and coherent states data structure(for ladder operator)
 */
MatrixXcd buildHamiltonianC(double lambda, MatrixXcd::Index oModes, cohStates c)
{
	MatrixXcd b = c.b;
	MatrixXcd b_dagger = c.b.adjoint();
	MatrixXcd H_C(2 * oModes, 2 * oModes);
	MatrixXcd x(oModes, oModes);

	x = b + b_dagger;
	H_C = kroneckerProduct(sigmaZ, x);

	return lambda / 2 * H_C;
}

/*
 * make bosonic x-operator matrix for oModes states
 */
MatrixXcd makeXOp(MatrixXcd::Index oModes)
{
	MatrixXcd b = createLadderOpF(false, oModes);
	MatrixXcd b_dagger = createLadderOpF(true, oModes);

	return b + b_dagger;
}

/*
 * make bosonic x²-operator matrix for oModes states
 */
MatrixXcd makeXSqOp(MatrixXcd::Index oModes)
{
	MatrixXcd b = createLadderOpF(false, oModes);
	MatrixXcd b_dagger = createLadderOpF(true, oModes);
	MatrixXcd ident = MatrixXcd::Identity(oModes, oModes);

	return b * b + b_dagger * b_dagger + 2 * b_dagger * b + ident;
}

/*
 * testing routines
 */

///* calculate relative errors for analytic "ana" and numeric "num" eigenstates for a given dimension "dim"
// *
// */
//template <typename Derived>
//MatrixXcd calcErrorR(MatrixXcd ana, const Eigen::MatrixBase<Derived> &num, MatrixXcd::Index dim)
//{
//	using std::abs;
//
//	MatrixXcd err(dim, dim);
//
//	MatrixXcd::Index i, j;
//
//	#pragma omp parallel private(i, j)
//	{
//		#pragma omp for schedule(dynamic, CHUNKSIZE)
//		for(i = 0; i < dim; i++)
//		{
//			for(j = 0; j < dim; j++)
//			{
//				err(i,j) = abs(num(i,j) - ana(i,j)) / abs(ana(i, j));
//			}
//		}
//	}
//
//	return err;
//}

///*
// * calculating absolute analytic "ana" and numeric "num" errors of the eigenstates for given matrix dimensions dim
// */
//template <typename Derived>
//MatrixXcd calcErrorA(MatrixXcd ana, const Eigen::MatrixBase<Derived> &num, MatrixXcd::Index dim)
//{
//	using std::abs;
//
//	MatrixXcd err(dim, dim);
//
//	MatrixXcd::Index i, j;
//
//	#pragma omp parallel private(i, j)
//	{
//		#pragma omp for schedule(dynamic, CHUNKSIZE)
//		for(i = 0; i < dim; i++)
//		{
//			for(j = 0; j < dim; j++)
//			{
//				err(i,j) = abs(num(i,j) - ana(i,j));
//			}
//		}
//	}
//
//	return err;
//}

///*
// * calculate first "number" numeric and analytic errors of the eigenenergies (absolute (abs_err) and relative(rel_err))
// */
//void calcError (complex<double> num[], complex<double> ana[], unsigned int number, complex<double> *abs_err, complex<double> *rel_err)
//{
//	using std::abs;
//
//	#pragma omp parallel for schedule(dynamic, CHUNKSIZE)
//	for (size_t i = 0; i < number; i++)
//	{
//		abs_err[i] = abs(num[i] - ana[i]);
//		rel_err[i] = abs_err[i] / abs(ana[i]);
//	}
//
//}

///*
// * print the first "number" analytic eigenvalues "energ" and numeric eigenvalues "n_ev"
// */
//template <typename Derived>
//void printEnergies(std::complex<double> *energ, unsigned int number, const Eigen::MatrixBase<Derived> &n_ev)
//{
//	using std::cout;
//	using std::endl;
//
//	cout << "analytisch:" << endl;
//	for(unsigned int i = 0; i < number; i++)
//	{
//		cout << "E_" << i << "_a = " << energ[i] << endl;
//	}
//	cout << "numerisch:" << endl;
//	for (typename Eigen::MatrixBase<Derived>::Index i = 0; i < n_ev.rows(); i ++)
//	{
//		cout << "E_" << i << "_n = " << (complex<double>) n_ev(i) << endl;
//	}
//
//}
//
//
//void printEnErrors (complex<double> babs_err[], complex<double> brel_err[], unsigned int oModes)
//{
//
//	using std::cout;
//	using std::endl;
//
//	cout << "absoluter Fehler" << endl;
//
//	for (size_t i = 0; i < oModes; i++)
//	{
//		cout << "ΔE_" << i << "_abs = " << babs_err[i] << endl;
//	}
//
//	cout << "relativer Fehler" << endl;
//
//	for (size_t i = 0; i < oModes; i++)
//	{
//		cout << "ΔE_" << i << "_rel = " << brel_err[i] << endl;
//	}
//
//}

/*
 * end of testing routine blocks
 */
void coherentBase(double delta, double epsilon, double lambda, double omega, size_t oModes, double displacement, std::string filename, double theta_max)
{
	using std::cout;
	using std::endl;
	using std::abs;
	using std::complex;
/*
 * testing and output routines within are commented out
 */

/*
 * building spin Hamiltonian
 */

//	cout << "H_Spin" << endl;
//	cout << "Δ = " << delta << ", ε = " << epsilon << endl << endl;

	MatrixS H_S = buildHamiltonianS(epsilon,delta);

//	cout << "H_S = " << endl << H_S << endl << endl;

//	// analytic eigenvalues
//	complex<double> sE_a [2];
//	sE_a[0] = - sqrt(epsilon * epsilon + delta * delta) / 2.0;
//	sE_a[1] = -sE_a[0];

//	//numeric eigenvalues
//	Eigen::SelfAdjointEigenSolver<MatrixS> d_H_S(H_S);
//	complex<double> sE_n [2];
//	sE_n[0] = d_H_S.eigenvalues()(0);
//	sE_n[1] = d_H_S.eigenvalues()(1);

//	//print out eigenvalues and their absolute and relative errors
//	cout << "Eigenwerte:" << endl;
//	printEnergies(sE_a, 2, d_H_S.eigenvalues());

//	complex<double> *srel_err = new complex<double>[2];
//	complex<double> *sabs_err = new complex<double>[2];
//	calcError(sE_n, sE_a, 2, sabs_err, srel_err);

////cout << "absoluter Fehler" << endl;
////cout << "ΔE_1_abs = " << sabs_err[0] << endl;
////cout << "ΔE_2_abs = " << sabs_err[1] << endl;
////cout << "relativer Fehler" << endl;
////cout << "ΔE_1_rel = " << srel_err[0] << endl;
////cout << "ΔE_2_rel = " << srel_err[1] << endl;

	// analytic and numeric eigenvectors

//	MatrixS s_ev;
//	MatrixS s_del_ev_r;
//	MatrixS s_del_ev_a;

	//calculating analytic coefficients α_+, α_-, β_+, β_-
	complex<double> alpha_p;
	complex<double> alpha_m;
	complex<double> beta_p;
	complex<double> beta_m;
	complex<double> renorm = exp(- lambda * lambda / 2 / omega / omega); // exp(-2θ²) = exp(-λ² / ω²)

	alpha_p = delta * renorm / sqrt(delta * delta * renorm * renorm + pow((epsilon - sqrt(epsilon * epsilon + delta * delta * renorm * renorm)), 2));
	beta_p = (-epsilon + sqrt(epsilon * epsilon + delta * delta * renorm * renorm)) / sqrt(delta * delta * renorm * renorm + pow((epsilon - sqrt(epsilon * epsilon + delta * delta * renorm * renorm)), 2));
	alpha_m = delta * renorm / sqrt(delta * delta * renorm * renorm + pow((epsilon + sqrt(epsilon * epsilon + delta * delta * renorm * renorm)), 2));
	beta_m = (-epsilon - sqrt(epsilon * epsilon + delta * delta * renorm * renorm)) / sqrt(delta * delta * renorm * renorm + pow((epsilon + sqrt(epsilon * epsilon + delta * delta * renorm * renorm)), 2));

//	s_ev(0, 0) = alpha_p;
//	s_ev(1, 0) = beta_p;
//	s_ev(0, 1) = alpha_m;
//	s_ev(1, 1) = beta_m;

	//calculating errors

//	for(MatrixS::Index i = 0; i < 2; i++)
//	{
//		for(MatrixS::Index j = 0; j < 2; j++)
//		{
//			s_del_ev_a(i, j) = abs(s_ev(i,j) - d_H_S.eigenvectors()(i,j));
//			s_del_ev_r(i, j) = s_del_ev_a(i, j) / abs(s_ev(i, j));
//		}
//	}
//
//	cout << endl << "Eigenvektoren" << endl;
//	cout << "analytische" << endl << s_ev << endl;
//	cout << "numerisch" << endl << d_H_S.eigenvectors() << endl;
//	cout << "absolute Fehlermatrix" << endl << s_del_ev_a << endl;
//	cout << "relative Fehlermatrix" << endl << s_del_ev_r << endl;

	/*
	 * bosonic hamiltonian
	 */

	cohStates c = createCohBase(oModes, displacement);
	MatrixXcd H_B = buildHamiltonianB(omega, oModes, c);

//	cout << endl << endl << "H_Bosonen" << endl << endl;
//	cout << "H_B = " << endl << H_B << endl << endl;

//	cout << "Eigenwerte" << endl;

	// analytic eigenvalues

//	complex<double> *bE_a = new complex<double>[oModes];

//	for(unsigned int i = 0; i < oModes; i++)
//	{
//		bE_a[i] = i;
//	}
//
	// numeric eigenvalues

//	Eigen::SelfAdjointEigenSolver<MatrixXcd> d_H_B(H_B);

//	printEnergies(bE_a, oModes, d_H_B.eigenvalues());

//	complex<double> *brel_err = new complex<double>[oModes];
//	complex<double> *babs_err = new complex<double>[oModes];
//	calcError(d_H_B.eigenvalues(), bE_a, oModes, babs_err, brel_err);

//	printEnErrors(babs_err, brel_err, oModes);

	// analytic eigenvalues
//	MatrixXcd b_ev = MatrixXcd::Identity(oModes, oModes);
//	cout << endl << "analytische Eigenvektoren" << endl << b_ev << endl;

	// numeric eigenvalues
//	MatrixXcd b_del_ev_r = MatrixXcd::Zero(oModes, oModes);
//	MatrixXcd b_del_ev_a = MatrixXcd::Zero(oModes, oModes);

//	b_del_ev_a = calcErrorA(b_ev, d_H_B.eigenvectors(), oModes);
//	b_del_ev_r = calcErrorR(b_ev, d_H_B.eigenvectors(), oModes);

//	cout << "numerische Eigenvektoren" << endl << d_H_B.eigenvectors() << endl;
//	cout << "absolute Fehlermatrix" << endl << b_del_ev_a << endl;
//	cout << "relative Fehlermatrix" << endl << b_del_ev_r << endl;

/*
 * SPIN-BOSON-HAMILTONIAN
 */

//	cout << endl << endl << "H_Spin_Bosonen" << endl;

	//building full Hamilton matrix
	MatrixXcd H_SB_B(2 * oModes, 2 * oModes);
	MatrixXcd H_SB_S(2 * oModes, 2 * oModes);
	MatrixXcd H_SB_C(2 * oModes, 2 * oModes);
	MatrixXcd H_B_I = MatrixXcd::Identity(oModes, oModes);
	MatrixS H_S_I = MatrixS::Identity();
	MatrixXcd H_SB_SB(2 * oModes, 2 * oModes);

	H_SB_C = buildHamiltonianC(lambda, oModes, c);

	H_SB_S = kroneckerProduct(H_S, H_B_I);
	H_SB_B = kroneckerProduct(H_S_I, H_B);

	H_SB_SB = H_SB_B + H_SB_S + H_SB_C;

//	cout << endl << H_SB_SB << endl;

	// analytic eigenvalues with correcture and numerical version

	Eigen::SelfAdjointEigenSolver<MatrixXcd> d_H_SB_SB(H_SB_SB);

	complex<double> *sbE_a = new complex<double> [1];
	complex<double> *sbE_n = new complex<double> [1];
	complex<double> E_g = lambda * lambda / 4 / omega;

	sbE_a[0] = - E_g - sqrt(epsilon * epsilon + delta * renorm * delta * renorm) / 2.0;
//	sbE_a[1] = - E_g + sqrt(epsilon * epsilon + delta * renorm * delta * renorm) / 2.0;

	sbE_n[0] = d_H_SB_SB.eigenvalues()(0);
//	sbE_n[1] = d_H_SB_SB.eigenvalues()(1);

//	cout << endl << "Eigenwerte" << endl;
//	cout << "analytisch" << endl;
//	cout << "E_0_a = " << sbE_a[0] << endl;
//	cout << "E_1_a = " << sbE_a[1] << endl;
//	cout << "numerisch" << endl;
//	cout << "E_0_n = " << sbE_n[0] << endl;
//	cout << "E_1_n = " << sbE_n[1] << endl;

//	cout << "all numerical eigenvalues" << endl << d_H_SB_SB.eigenvalues() << endl;

//	//calculating errors
//
//	complex<double> *sbabs_err = new complex<double> [4];
//	complex<double> *sbrel_err = new complex<double> [4];
//
//	calcError(sbE_n, sbE_a, 4, sbabs_err, sbrel_err);
//
//	//cout << "absoluter Fehler" << endl;
//	//cout << "ΔE_+_0 = " << sbabs_err[0] << endl;
//	//cout << "ΔE_+_1 = " << sbabs_err[1] << endl;
//	//cout << "ΔE_-_0 = " << sbabs_err[2] << endl;
//	//cout << "ΔE_-_1 = " << sbabs_err[3] << endl;
//	//cout << "relativer Fehler" << endl;
//	//cout << "ΔE_+_0 = " << sbrel_err[0] << endl;
//	//cout << "ΔE_+_1 = " << sbrel_err[1] << endl;
//	//cout << "ΔE_-_0 = " << sbrel_err[2] << endl;
//	//cout << "ΔE_-_1 = " << sbrel_err[3] << endl;
//
//	// analytic eigenvectors with correcture and output of numerical and analytic version
//
//	MatrixXcd ev_a_SB (2 * oModes, 2 * oModes);
//	MatrixXcd ev_a_SB_rel (2 * oModes, 2 * oModes);
//	MatrixXcd ev_a_SB_abs (2 * oModes, 2 * oModes);
//
//	ev_a_SB = kroneckerProduct(d_H_S.eigenvectors(), d_H_B.eigenvectors());
//
////cout << endl << "Eigenvektoren" << endl;
////cout << "analytisch vor Störung" << endl << ev_a_SB << endl;
//
//	tmp_p /= omega;
//	tmp_m /= omega;
//	ev_a_SB.block(0, 0, 2 * oModes, 1) +=  tmp_p * ev_a_SB.block(0, 1, 2 * oModes, 1);
//	ev_a_SB.block(0, 1, 2 * oModes, 1) -= tmp_p * ev_a_SB.block(0, 0, 2 * oModes, 1);
//	ev_a_SB.block(0, oModes, 2 * oModes, 1) += tmp_m * ev_a_SB.block(0, oModes + 1, 2 * oModes, 1);
//	ev_a_SB.block(0, oModes + 1, 2 * oModes, 1) -= tmp_m * ev_a_SB.block(0, oModes, 2 * oModes, 1);

////cout << "analytisch mit Störung:" << endl;
////cout << ev_a_SB << endl;
////cout << "numerisch:" << endl;
////cout << d_H_SB_SB.eigenvectors() << endl;
//
//	//calculating errors
//
//	ev_a_SB_abs = calcErrorA(ev_a_SB, d_H_SB_SB.eigenvectors(), 4);
//	ev_a_SB_rel = calcErrorR(ev_a_SB, d_H_SB_SB.eigenvectors(), 4);

////	cout << "absoluter Fehler der Eigenvektoren" << endl;
////	cout << ev_a_SB_abs << endl;
////	cout << "relativer Fehler der Eigenvektoren" << endl;
////	cout << ev_a_SB_rel << endl;

///*
// * expectation values <x> and <x²>
// */
//
//	MatrixXcd x = makeXOp(oModes);
//	MatrixXcd x_sq = makeXSqOp(oModes);
//	MatrixXcd x_SB (2 * oModes, 2 * oModes);
//	MatrixXcd x_sq_SB (2 * oModes, 2 * oModes);
//	complex<double> exp_x_p_a ;
//	complex<double> exp_x_m_a;
//	complex<double> exp_x_p_n;
//	complex<double> exp_x_m_n;
//	complex<double> exp_x_p_re;
//	complex<double> exp_x_m_re;
//	complex<double> exp_x_p_ae;
//	complex<double> exp_x_m_ae;
//	complex<double> exp_x_sq_p_a;
//	complex<double> exp_x_sq_m_a;
//	complex<double> exp_x_sq_p_n;
//	complex<double> exp_x_sq_m_n;
//	complex<double> exp_x_sq_p_re;
//	complex<double> exp_x_sq_m_re;

//	//transforming x and x² to Spin-Boson Hilbert space

//	x_SB = kroneckerProduct(H_S_I, x);
//	x_sq_SB = kroneckerProduct(H_S_I, x_sq);

//	//calculating analytic and numeric version von <x>

//	exp_x_p_a = lambda / omega * (alpha_p * alpha_p - beta_p * beta_p);
//	exp_x_m_a = lambda / omega * (alpha_m * alpha_m - beta_m * beta_m);
//
//	exp_x_p_n = expVF(d_H_SB_SB.eigenvectors().col(0), x_SB);
//	exp_x_m_n = expVF(d_H_SB_SB.eigenvectors().col(1), x_SB);

//	cout << "<x_0_+>_a = " << exp_x_p_a << endl;
//	cout << "<x_0_->_a = " << exp_x_m_a << endl;

//	cout << "numerisch" << endl;
//	cout << "<x_0_+>_n = " << exp_x_p_n << endl;
//	cout << "<x_0_->_n = " << exp_x_m_n << endl;

//	//calculating errors for <x>

//	exp_x_0_p_ae = abs(exp_x_0_p_a - exp_x_0_p_n);
//	exp_x_1_p_ae = abs(exp_x_1_p_a - exp_x_1_p_n);
//	exp_x_0_m_ae = abs(exp_x_0_m_a - exp_x_0_m_n);
//	exp_x_1_m_ae = abs(exp_x_1_m_a - exp_x_1_m_n);

//	exp_x_0_p_re = abs(exp_x_0_p_a - exp_x_0_p_n) / abs(exp_x_0_p_a);
//	exp_x_1_p_re = abs(exp_x_1_p_a - exp_x_1_p_n) / abs(exp_x_1_p_a);
//	exp_x_0_m_re = abs(exp_x_0_m_a - exp_x_0_m_n) / abs(exp_x_0_m_a);
//	exp_x_1_m_re = abs(exp_x_1_m_a - exp_x_1_m_n) / abs(exp_x_1_m_a);

////cout << "absoluter Fehler" << endl;
////cout << "Δ<x_0_+> = " << exp_x_0_p_ae << endl;
////cout << "Δ<x_1_+> = " << exp_x_1_p_ae << endl;
////cout << "Δ<x_0_-> = " << exp_x_0_m_ae << endl;
////cout << "Δ<x_1_-> = " << exp_x_1_m_ae << endl;

////cout << "relative Fehler" << endl;
////cout << "Δ<x_0_+> = " << exp_x_0_p_re << endl;
////cout << "Δ<x_1_+> = " << exp_x_1_p_re << endl;
////cout << "Δ<x_0_-> = " << exp_x_0_m_re << endl;
////cout << "Δ<x_1_-> = " << exp_x_1_m_re << endl;

	//calculating analytic and numeric version of <x²>

//	exp_x_sq_p_a = 1 + lambda * lambda / omega / omega;
//	exp_x_sq_m_a = exp_x_sq_p_a;
//
//	exp_x_sq_p_n = expVF(d_H_SB_SB.eigenvectors().col(0), x_sq_SB);
//	exp_x_sq_m_n = expVF(d_H_SB_SB.eigenvectors().col(1), x_sq_SB);

//	cout << endl << "Erwartungswert x²" << endl;
//	cout << "analytisch" << endl;
//	cout << "<x_0^2_+>_a = " << exp_x_sq_p_a << endl;
//	cout << "<x_0^2_->_a = " << exp_x_sq_m_a << endl;
//
//	cout << "numerisch" << endl;
//	cout << "<x_0^2_+>_n = " << exp_x_sq_p_n << endl;
//	cout << "<x_0^2_->_n = " << exp_x_sq_m_n << endl;

//	//calculating errors for <x²>
//
//	exp_x_sq_0_p_ae = abs(exp_x_sq_0_p_a - exp_x_sq_0_p_n);
//	exp_x_sq_1_p_ae = abs(exp_x_sq_1_p_a - exp_x_sq_1_p_n);
//	exp_x_sq_0_m_ae = abs(exp_x_sq_0_m_a - exp_x_sq_0_m_n);
//	exp_x_sq_1_m_ae = abs(exp_x_sq_1_m_a - exp_x_sq_1_m_n);
//
//	exp_x_sq_0_p_re = abs(exp_x_sq_0_p_a - exp_x_sq_0_p_n) / abs(exp_x_sq_0_p_a);
//	exp_x_sq_1_p_re = abs(exp_x_sq_1_p_a - exp_x_sq_1_p_n) / abs(exp_x_sq_1_p_a);
//	exp_x_sq_0_m_re = abs(exp_x_sq_0_m_a - exp_x_sq_0_m_n) / abs(exp_x_sq_0_m_a);
//	exp_x_sq_1_m_re = abs(exp_x_sq_1_m_a - exp_x_sq_1_m_n) / abs(exp_x_sq_1_m_a);
//
////cout << "absoluter Fehler" << endl;
////cout << "Δ<x²_0_+> = " << exp_x_sq_0_p_ae << endl;
////cout << "Δ<x²_1_+> = " << exp_x_sq_1_p_ae << endl;
////cout << "Δ<x²_0_-> = " << exp_x_sq_0_m_ae << endl;
////cout << "Δ<x²_1_-> = " << exp_x_sq_1_m_ae << endl;

////cout << "relative Fehler" << endl;
////cout << "Δ<x²_0_+> = " << exp_x_sq_0_p_re << endl;
////cout << "Δ<x²_1_+> = " << exp_x_sq_1_p_re << endl;
////cout << "Δ<x²_0_-> = " << exp_x_sq_0_m_re << endl;
////cout << "Δ<x²_1_-> = " << exp_x_sq_1_m_re << endl;

/*
 * write to file
 */
	FILE *file = fopen(filename.c_str(), "a");
//	fprintf(file, "%s %.15f %.15f %15f %.15f %.15f %.15f %.15f %.15f %.15f\n", "   ", theta_max, sbE_a[0].real(), sbE_n[0].real(), sbE_a[1].real(), sbE_n[1].real(), exp_x_sq_p_a.real(), exp_x_sq_p_n.real(), exp_x_sq_m_a.real(), exp_x_sq_m_n.real());
	fprintf(file, "%s %.15f %.15f %15f\n", "   ", theta_max, sbE_a[0].real(), sbE_n[0].real());
	fclose(file);

}

int main()
{
	Eigen::initParallel();

	using std::string;
	using std::to_string;
	using std::cout;
	using std::endl;

	// parameter scan in θ for Δ = 0.15, ε = 0, ω = 1, N = {11}
	size_t N_10[] = {11};
	double delta = 0.15;
	double lambda = 5;
	double omega= 1;
	double x_max = 5 * lambda / 6 / omega;
	cout << x_max << endl;
	double theta_min = x_max - 0.20 * x_max;
	double theta_max = x_max + 0.85 * x_max;
	double epsilon = 0;
	unsigned int repetition = 1000;

	double h = (theta_max - theta_min) / (double) repetition;

	for(auto j : N_10)
	{

		std::string filename = ("build/data/coherent-states_pm-scan_in_θ_max_Δ=3_ω=20_ε=0_λ=100_N=" + to_string(j) +  ".txt");
		FILE *file = fopen(filename.c_str(), "w");
		fprintf(file, "%1s %15s %15s %15s\n", "# ", "λ", "E_p_a", "E_p_n");
		fclose(file);

		for (unsigned int i = 0; i <= repetition; i++)
		{
			double theta_curr = theta_min + h * i;
			double displacement = theta_curr / (j - 1);

			cout << "Coherent States tunneling variation: N = " << j << " | " << i << " / " << repetition << " iterations" << endl;
			coherentBase(delta, epsilon, lambda, omega, j, displacement, filename, theta_curr);
		}
	}
}
