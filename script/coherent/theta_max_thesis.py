import numpy as np
import matplotlib.pyplot as plt

θ_max, E_p_a, E_p_n_2 = np.loadtxt("build/data/coherent-states_pm-scan_in_θ_max_Δ=3_ω=20_ε=0_λ=100_N=11.txt", skiprows=1, unpack=True)

fig = plt.figure()
ax1 = fig.add_subplot(1, 1, 1)
ax1.plot(θ_max, E_p_a, 'r-', label='störungstheoretisch')
ax1.plot(θ_max, E_p_n_2, 'g-', label='numerisch, $N=5$')
ax1.axvline(x=4.99999,  label='Minimum von $E_0$', c='m', ls='--')
ax1.axvline(x=25 / 6, label=r'$θ_\t{max} = \frac{5\bar{λ}}{6ω_0}$', c='b', ls='--')
ax1.set_xlabel(r'$θ_{\t{max}}$')
ax1.set_ylabel(r'$E_0 / ω_0$')
ax1.set_xlim(4, 5.68)
ax1.set_ylim(-6.255, -6.240)
ax1.legend(loc='upper right')
fig.savefig('build/graphic/coherent/minimum_thesis.pdf')
