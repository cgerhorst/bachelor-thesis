import matplotlib.pyplot as plt
import numpy as np

Δf, E_a, E_p_f_3 = np.loadtxt("build/data/pm-scan_in_Δ_ω=20_ε=0_λ=100_N=24.txt", skiprows=1, usecols=(0, 1, 2), unpack=True)
_, E_p_c_3 = np.loadtxt("build/data/coherent-states_pm-scan_in_Δ_ω=20_ε=0_λ=100_θmax=5_N=3.txt", skiprows=1, usecols=(0, 2), unpack=True)
_, E_p_c_5 = np.loadtxt("build/data/coherent-states_pm-scan_in_Δ_ω=20_ε=0_λ=100_θmax=5_N=5.txt", skiprows=1, usecols=(0, 2), unpack=True)
_, E_p_c_7 = np.loadtxt("build/data/coherent-states_pm-scan_in_Δ_ω=20_ε=0_λ=100_θmax=5_N=7.txt", skiprows=1, usecols=(0, 2), unpack=True)
Δc,E_p_c_11 = np.loadtxt("build/data/coherent-states_pm-scan_in_Δ_ω=20_ε=0_λ=100_θmax=5_N=11.txt", skiprows=1, usecols=(0, 2), unpack=True)

fig = plt.figure()
ax2= fig.add_subplot(1, 1, 1)
ax2.plot(Δf, E_a, 'c-', label='störungstheoretisch')
ax2.plot(Δf, E_p_f_3, 'y-', label='Fockbasis, $N=24$')
ax2.plot(Δc, E_p_c_3, 'b:', label='kohärente Basis, $N=1$')
ax2.plot(Δc, E_p_c_5, 'g--', label='kohärente Basis, $N=2$')
ax2.plot(Δc, E_p_c_7, 'm:', label='kohärente Basis, $N=3$')
ax2.plot(Δc, E_p_c_11, 'r--', label='kohärente Basis, $N=5$')
ax2.legend(loc='lower left')
ax2.set_xlabel(r'$Δ / ω_0$')
ax2.set_ylabel(r'$E_0 / ω_0$')
ax2.yaxis.get_major_formatter().set_useOffset(False)
fig.savefig('build/graphic/coherent/E_thesis.pdf')
