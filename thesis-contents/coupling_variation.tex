Eine gute Näherung der Numerik an die Analytik ist bereits für wenige bosonische Zustände zu erwarten, da nur die niedrigsten Energien betrachtet werden und die betrachteten bosonischen Leiteroperatoren maximal mit je zwei aufeinanderfolgenden benachbarten Anregungen vermitteln.
Der Hamiltonoperator und $x = B_0^{} + \Dagger{B_0}$ beinhalten nur Leiteroperatoren, die mit den benachbarten Anregungen vermitteln, jedoch enthält~$x^2$~\eqref{eqn:x^2} Terme, die nicht nur mit der nächsten, sondern auch der übernächsten benachbarten Anregung vermitteln.
Daher sind hier ist mehr Zustände für eine gute Beschreibung zu erwarten.

Als Referenz für das Erreichen einer guten Näherung im nicht-analytischen Bereich dient daher eine Fockbasis mit $N = 100$ Zuständen.
In der Numerik sind in allen Abbildungen dieses Abschnitts Fockbasen mit verschiedener Anzahl an $N$ Zuständen gegenübergestellt.
Alle Berechnungen in diesem Kapitel sind mit den Parametern $ε / ω_0 = Δ / ω_0 = 0.1$ in Einheiten von $ω_0$ durchgeführt.

\begin{figure}
   \includegraphics[scale=0.78]{build/graphic/λ/E_thesis.pdf}
   \caption{Vergleich von störungstheoretischer \eqref{eqn:energies_pertubationcoupling} und numerischer Berechnung der Grundzustandsenergie $E_0$ und der 1. Anregungsenergie $E_1$ als Funktion der Kopplungsstärke~$\bar{λ}^2$ in Einheiten der Cutoff-Energie $ω_0$. In der Numerik werden Fockbasen mit verschiedener Anzahl an $N$ Zuständen gegenübergestellt. Die Energien werden für die Energieaufspaltung und Tunnelamplitude $ε / ω_0 = Δ / ω_0 = 0.1$ berechnet.}
   \label{fig:λ-E}
\end{figure}
 In Abbildung \ref{fig:λ-E} wird die störungstheoretische \eqref{eqn:energies_pertubationcoupling} mit der numerischen Berechnung der Grundzustandsenergie $E_0$ und der 1. Anregungsenergie $E_1$ als Funktion der quadratischen Kopplungsstärke $\bar{λ}^2$ verglichen.
 Es wird ersichtlich, dass im Gültigkeitsbereich der Störungsrechnung die Analytik exakt mit der Numerik zusammenpasst, die Störungsrechnung ist sogar eine gute Näherung bis \mbox{$\bar{λ} / ω_0 \approx 0.8$}.
Bis $\bar{λ} / ω_0 \approx 0.4$ sind $N = 2$, bis $\bar{λ} / ω_0 \approx 0.85$ sind $N = 3$ Zustände eine gute Näherung an den unendlichen Hilbertraum der Analytik.
Die Kurve für $N = 4$ liegt im untersuchten Parameterbereich deckungsgleich zu der Referenzkurve für $N = 100$.
Die beiden Energien werden schlauchartig abgesenkt und stoßen sich nicht ab.
Die Analytik beschreibt eine Energieabsenkung mit $\LandauO(\bar{λ}^2)$.
Die Numerik korrigiert diese außerhalb des Gültigkeitsbereiches der Störungsrechnung hin zu kleineren Werten und erhöht die Krümmung.

\begin{figure}
  \includegraphics[scale=0.78]{build/graphic/λ/expX_thesis.pdf}
  \caption{Vergleich von störungstheoretischer \eqref{eqn:weak_x} und numerischen Berechnung der Erwartungswerte $\avg{x}_0$ und $\avg{x}_1$ als Funktion der Kopplung $\bar{λ}$ in Einheiten der Cutoff-Energie $ω_0$. In der Numerik werden Fockbasen mit verschiedener Anzahl an $N$ Zuständen gegenübergestellt. Die Erwartungswerte werden für die Energieaufspaltung und Tunnelamplitude $ε / ω_0= Δ / ω_0 = 0.1$ mit dem Grund- und dem 1. angeregten Zustand berechnet.}
  \label{fig:λ-<x>}
\end{figure}
In Abbildung \ref{fig:λ-<x>} wird die störungstheoretische \eqref{eqn:weak_x} und die numerischen Berechnung der Erwartungswerte $\avg{x}_0$ und $\avg{x}_1$ als Funktion der Kopplung $\bar{λ}$ in Einheiten von $ω_0$ verglichen.
Die Numerik geht bei der mittleren Verschiebung im Grenzfall $\bar{λ} / ω_0 \rightarrow 0$ in die Analytik über und stimmt bis $\bar{λ} / ω_0 \approx 0.35$ gut überein.
Zwei Zustände reichen bis $\bar{λ} / ω_0 \approx 0.15$, drei Zustände bis $\bar{λ} / ω_0 \approx 0.6$ für eine gute Beschreibung; für vier Zustände gibt es erst bei $\bar{λ} / ω_0 \approx 1$ einen kleine Abweichung von der Referenzkurve mit $N = 100$.
Für kleine Tunnelamplituden $Δ / ω_0 = 0.1$ verändern die Eigenzustände aus der exakten Diagonalisierung die Auslenkung $\avg{x}$ negativ für den Grundzustand und positiv für den ersten angeregten Zustand mit wachsender Kopplung $\bar{λ}$.
Obwohl die Zustände bei der exakten Diagonalisierung für $Δ / ω_0 \neq 0$  mischen, bleibt die Auslenkungsrichtung, die sich für die exakte Lösung bei $Δ = 0$ ergibt, im untersuchten Bereich erhalten.
Jedoch sind die Verschiebungen betragsmäßig geringer als für den Fall $Δ / ω_0 = 0$, bei dem der Hamiltonoperator in zwei Unterräume zerfällt.
Das analytische Ergebnis \eqref{eqn:weak_x} zeigt, dass der Erwartungswert linear von $\bar{λ}$ abhängt; dies gilt für kleine $\bar{λ}$. Für $\bar{λ} / ω_0 > 0.4$ zeigt die Numerik eine Krümmung.

\begin{figure}
  \includegraphics[scale=0.78]{build/graphic/λ/expX2_thesis.pdf}
  \caption{Vergleich von störungstheoretischer \eqref{eqn:weak_x2} und numerischen Berechnung der Erwartungswerte $\avg{x^2}_0$ und $\avg{x^2}_1$ als Funktion der Kopplung $\bar{λ}$ in Einheiten der Cutoff-Energie $ω_0$. In der Numerik werden Fockbasen mit verschiedener Anzahl an $N$ Zuständen gegenübergestellt. Die Erwartungswerte werden für die Energieaufspaltung und Tunnelamplitude $ε / ω_0= Δ / ω_0 = 0.1$ mit dem Grund- und dem 1. angeregten Zustand berechnet.}
  \label{fig:λ-<x²>}
\end{figure}
In Abbildung \ref{fig:λ-<x²>} wird die störungstheoretische \eqref{eqn:weak_x2} und die numerischen Berechnung der Erwartungswerte $\avg{x^2}_0$ und $\avg{x^2}_1$ als Funktion von $\bar{λ}$ in Einheiten von $ω_0$ verglichen.
Numerik und Analytik stimmen im Gültigkeitsbereich der analytischen Näherung überein, die Abweichung bleibt gering bis $\bar{λ} / ω_0 \approx 0.15$.
Weiterhin bieten bis $\bar{λ} / ω_0 \approx 0.35$ drei Zustände, bis $\bar{λ} / ω_0 \approx 0.8$ vier Zustände eine gute Beschreibung der Physik.
Für den ganzen Parameterbereich werden jedoch $N = 5$ Zustände, also wie erwartet einer mehr als vorher, benötigt, um eine gute Approximation des unendlich dimensionalen Hilbertraums der Analytik zu gewährleisten.
Die Störungsrechnung wird durch die Numerik hin zu einer größeren Krümmung korrigiert, bleibt aber stets besser als die Numerik mit $N = 2$ Zuständen.
Da $\avg{x^2}$ unabhängig von der Verschiebungsrichtung ist, sehen die Kurven für $\avg{x^2}_0$ und $\avg{x^2}_1$ annäherend gleich aus.
Die Differenz $\avg{x^2}_1 - \avg{x^2}_0$ ist im Vergleich zur Abbildung \ref{fig:Δ-<x^2>} kleiner.
