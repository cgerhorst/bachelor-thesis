Beim Spin-Boson-Modell mit dem Hamiltonoperator \cite{PhysRevB.71.045122}
\begin{eqn}
  \mathcal{H}_{\t{SB}}^{} = \underbrace{\rule[-0.955\baselineskip]{0pt}{\baselineskip} -\frac{Δ}{2} σ_x^{}}_{\mathcal{H}_{\t{T}}} + \underbrace{\rule[-0.94\baselineskip]{0pt}{\baselineskip} \frac{ε}{2} σ_z^{}}_{\mathcal{H}_{\t{G}}} + \underbrace{\rule[-0.9\baselineskip]{0pt}{\baselineskip}\sum_i ω_i^{} \Dagger{b_i}b_i^{}}_{\mathcal{H_{\t{B}}}} + \underbrace{σ_z^{} \sum_i \frac{λ_i^{}}{2} \g(b_i^{} + \Dagger{b_i})}_{\mathcal{H}_{\t{C}}} \eqlabel{eqn:full-hamiltonian}
\end{eqn}
koppelt ein Spin, als Freiheitsgrad, an ein Bad von harmonischen Oszillatoren.

Die Interpretation des Spin-Boson-Modells \cite{RevModPhys.59.1} ist in Abbildung \ref{fig:spin-boson-interpretation} in Form eines Doppelmuldenpotentials veranschaulicht.
Jedem harmonischen Oszillator $i$ mit der Frequenz~$ω_i$, der mit der Kopplung $λ_i$ an die $z$-Komponente eines Spins koppelt, wird ein harmonisches Potential mit Minima an der Stelle $+ λ_i / 2 ω_i$, für $σ_z = 1$, und $- λ_i / 2 ω_i$, für $σ_z = -1$, in einer Potentiallandschaft zugewiesen \cites{RevModPhys.59.1}{PhysRevB.71.045122}.
Abhängig von der Kopplung~$λ_i$ werden die Oszillatoren mit fester Frequenz $ω_i$ entlang der $θ$-Achse verschoben~\cite{PhysRevB.71.045122}.
Außerdem sind die Energieniveaus der linken und rechten parabolischen Mulden um $ε$ aufgespalten; dies wird in \eqref{eqn:full-hamiltonian} durch den Term $\mathcal{H}_{\t{G}}$ beschrieben.
Bei einer endlichen Tunnelamplitude $Δ$ in $\mathcal{H}_{\t{T}}$ wird zwischen den beiden Minima vermittelt.
\begin{figure}
  \scalebox{0.67}{\input{thesis-contents/interpretation_spin-boson.tex}}
  \caption{Interpretation des Spin-Boson-Modells, exemplarisch veranschaulicht für einen harmonischen Oszillator $i$. Die Energien in den Minima sind um $ε$ aufgespalten. Der harmonische Oszillator der Frequenz $ω_i$ koppelt mit $λ_i$ an einen Spin; dies führt zu einer Verschiebung nach links ($σ_z = -1$) und rechts ($σ_z = 1$). Bei einer endlichen Tunnelamplitude~$Δ$ wird zwischen den beiden Minima vermittelt. (entnommen und angepasst aus~\cite{RevModPhys.59.1}).}
  \label{fig:spin-boson-interpretation}
\end{figure}

Betrachtet werden nur die Grundzustände der Minima; es ergibt sich ein Zwei-Niveau-System, das in \eqref{eqn:full-hamiltonian} durch die Paulimatrizen $σ_x$ und $σ_z$  repräsentiert wird \cite{PhysRevB.71.045122}.
Dessen Dynamik wird vor allem durch die Konkurrenz der Tunnelamplitude $Δ$ im Tunnelterm $\mathcal{H}_{\t{T}}$ und des Reibungsterms $λ_i^{}\g(b_i^{} + \Dagger{b}_i)$, mit den bosonischen Leiteroperatoren $b_i^{}$ und $\Dagger{b}_i$, im Kopplungsterm $\mathcal{H}_{\t{C}}$ geprägt \cite{PhysRevB.71.045122}.
Der Kopplungsterm $\mathcal{H}_{\t{C}}$ beschreibt die lineare Kopplung jedes harmonischen Oszillators $i$ des Bades $\mathcal{H}_{\t{B}}$ an die $z$-Komponente des Spins \cite{PhysRevB.71.045122}.
Diese Kopplung wird vollständig durch die Badspektralfunktion
\begin{eqn}
  \f{J}(ω) = \PI \sum_i λ_i^2 \Diracdelta(ω_i^{} - ω) \IEEEyesnumber \IEEEyessubnumber \eqlabel{eqn:bath-spectral}
\end{eqn}
mit Kopplung $λ_i$ charakterisiert, für die bei niedrigen Energien die Standardnäherung
\begin{eqn}
  \f{J}(ω) = 2 \PI α ω_{\t{c}}^{1-s} ω^s\pns, \;\;\; 0 < ω < ω_{\t{c}}\pns, \;\;\; s > -1\pns, \IEEEyessubnumber \eqlabel{eqn:stand_bath-spectral}
\end{eqn}
mit der Cutoff-Energie $ω_{\t{c}}$ und der dimensionslosen Dissipationsstärke $α$, gilt \cite{PhysRevB.71.045122}.
Mit dem Exponenten $s$  wird das bosonische Bad kategorisiert \cite{PhysRevB.71.045122}.

Das Spin-Boson-Modell erlaubt eine Anwendung auf viele physikalische und chemische Systeme, die durch eine generalisierte Koordinate beschrieben werden, der ein effektives Energiepotential mit zwei Minima auf ungefähr dem selben Niveau zugeordnet werden kann \cite[Kap. 3.2]{weis1999quantum}.
In \cite{PhysRevB.71.045122} wird eine Übersicht über einige der interdisziplinären Anwendungen des Modells gegeben.
Das Modell wird neben quantendissipativen Systemen auch bei der Untersuchung von Qubits oder des Ladungstransfers in Molekülen angewendet.
Die numerische Untersuchung des Modells wird wegen entscheidenden Vorteilen gegenüber bisherigen Verfahren (siehe \cite{PhysRevB.71.045122}) heutzutage vorzugsweise mit der numerischen Renormierungsgruppe durchgeführt.
