Der vollständige Spin-Boson Hamiltonoperator $\mathcal{H}_\t{SB}$ \eqref{eqn:full-hamiltonian} kann nicht exakt diagonalisiert werden, da
\begin{eqn}
  \commut{σ_x}{σ_z} = - 2 \I σ_{y} \Rightarrow \commut{σ_x}{\mathcal{H}_{\t{SB}}} \neq 0\pns.
\end{eqn}
Aus diesem Grund wird $\mathcal{H}_{\t{T}}$ jetzt vernachlässigt und später bei der Behandlung der 1.~Wilson-Schale in Kapitel \ref{pertubation gaptunneling} als Störung betrachtet.
Der verbleibende Hamiltonoperator
\begin{eqn}
  \mathcal{H}^0_{\t{SB}} = \frac{ε}{2} σ_z^{} + \sum_i ω_i^{} \Dagger{b_i} b_i^{} + \frac{σ_z}{2} \sum_i λ_i^{} (b_i^{} + \Dagger{b_i}) \eqlabel{eqn:no-tunneling-hamiltonian}
\end{eqn}
kann diagonalisiert werden, weil der Kommutator $\commut{σ_z^{}}{\mathcal{H}^0_{\t{SB}}} = 0$ ist.
\begin{proof}[Beweis]
  Einteilchenoperatoren wie $σ_z$ und die bosonischen Operatoren $b^{}$ und $\Dagger{b}$, wirken immer nur in ihren eigenen Hilberträumen \cite[Kap. 5.2.3]{nolting_5.2}, sodass
  \begin{eqn}
    \commut{σ_z^{}}{b} = \commut{σ_z^{}}{\Dagger{b}} = 0\pns. \eqlabel{eqn:commut(σ,b)}
  \end{eqn}
  Für Kommutatoren mit beliebigen Operatoren $A$, $B$ und $C$ gilt die Produktregel
  \begin{eqn}
    \commut{A}{B C} = B \commut{A}{C} + \commut{A}{B} C\pns. \eqlabel{eqn:product-rule-commutator}
  \end{eqn}
  Mit \eqref{eqn:commut(σ,b)} und \eqref{eqn:product-rule-commutator} folgt daher
  \begin{eqn}
    \commut{σ_z^{}}{\mathcal{H^0_{\t{SB}}}} = \frac{ε}{2} \commut{σ_z^{}}{σ_z^{}} + \sum_i ω_i^{} \commut{σ_z^{}}{\Dagger{b_i} b_i^{}} + \sum_i \frac{λ_i^{}}{2} \g(\commut{σ_z^{}}{σ_z^{} b_i^{}} + \commut{σ_z^{}}{σ_z^{} \Dagger{b_i}}) = 0\pns, \IEEEeqnarraynumspace
  \end{eqn}
  da die einzelnen Summanden verschwinden.
\end{proof}
Das impliziert, dass $σ_z$ hier eine Erhaltungsgröße, also auch der Generator einer Symmetrietransformation ist, die $\mathcal{H^0_{\t{SB}}}$ invariant lässt.
Der Hamiltonoperator zerfällt daher in zwei orthogonale Unterräume $\mathcal{H}_{↑}$ und $\mathcal{H}_{↓}$, die zu den Eigenzuständen $\ket{↑}$ und $\ket{↓}$ von $σ_z$ gehören, und kann dort getrennt betrachtet werden.

Im Hamiltonoperator \eqref{eqn:no-tunneling-hamiltonian} liefern die Leiteroperatoren in $\mathcal{H}_{\t{C}}$ Nebendiagonalelemente, da sie zwischen verschiedenen bosonischen Anregungen vermitteln.
Der Kopplungsterm kann jedoch durch Umformung eliminiert werden (vgl. \cite[I E]{PhysRevB.71.045122}):
\begin{eqns}
  \IEEEyesnumber \eqlabel{eqn:displaced_operators} \IEEEnonumber
  \mathcal{H}^0_{↑/↓} &=& \pm \frac{ε}{2} + \sum_i ω_i^{} \Dagger{b_i}b_i^{} \pm \frac{1}{2} \sum_i λ_i^{} \g(b_i^{} + \Dagger{b_i}) \IEEEyesnumber \IEEEyessubnumber*\\
                    &=& \pm \frac{ε}{2} + \sum_i ω_i^{} \Dagger{b_i}b_i^{} \pm \frac{1}{2} \sum_i λ_i^{} \g(b_i^{} + \Dagger{b_i}) + \sum_i \g(\frac{λ_i^2}{4 ω_i} - \frac{λ_i^2}{4 ω_i}) \\
                    &=& \pm \frac{ε}{2} + \sum_i \g(ω_i^{} \g(\Dagger{b_i}b_i^{} \pm \frac{1}{2}  \frac{λ_i}{ω_i} \g(b_i^{} + \Dagger{b_i}) + \frac{λ_i^2}{4 ω_i^2}) - \frac{λ_i^2}{4 ω_i}) \\
                    &=& \pm \frac{ε}{2} + \sum_i \g{(}{\vphantom{ω_i \g(\Dagger{b_i} \pm \frac{λ_i}{2 ω_i}) \g(b_i \pm \frac{λ_i}{2 ω_i}) - \frac{λ_i^2}{4 ω_i}}}{.} ω_i^{} \g{(}{\vphantom{\Dagger{b_i} \pm \frac{λ_i}{2 ω_i}}}{.}\Dagger{b_i} \underbrace{\pm \frac{λ_i}{2 ω_i}}_{\equiv θ_i^{↑/↓}}\g{.}{\vphantom{\Dagger{b_i} \pm \frac{λ_i}{2 ω_i}}}{)} \g{(}{\vphantom{b_i^{} \pm \frac{λ_i}{2 ω_i}}}{.}b_i^{} \underbrace{\pm \frac{λ_i}{2 ω_i}}_{\equiv θ_i^{↑/↓}}\g{.}{\vphantom{b_i^{} \pm \frac{λ_i}{2 ω_i}}}{)} - \frac{λ_i^2}{4 ω_i}\g{.}{\vphantom{ω_i \g(\Dagger{b_i} \pm \frac{λ_i}{2 ω_i}) \g(b_i \pm \frac{λ_i}{2 ω_i}) - \frac{λ_i^2}{4 ω_i}}}{)} \\
                    &=& ± \frac{ε}{2} + \sum_i \g{(}{\vphantom{\g(\Dagger{b_i} + θ_i^{↑/↓}) + \g(b_i + θ_i^{↑/↓}) - \frac{λ_i^2}{4 ω_i}} ω_i^{}}{.}\g{(}{\vphantom{\Dagger{b_i} + θ_i^{↑/↓} + b_i^{} + θ_i^{↑/↓}}}{.}\underbrace{\Dagger{b_i} + θ_i^{↑/↓}}_{\equiv \Dagger{\bar{b}_i}}\g{.}{\vphantom{\Dagger{b_i} + θ_i^{↑/↓} + b_i^{} + θ_i^{↑/↓}}}{)} \g{(}{\vphantom{\Dagger{b_i} + θ_i^{↑/↓} + b_i^{} + θ_i^{↑/↓}}}{.}\underbrace{\vphantom{\Dagger{b_i} + θ_i^{↑/↓}} b_i^{} + θ_i^{↑/↓}}_{\equiv \bar{b}_i\vphantom{\Dagger{b_i}}}\g{.}{\vphantom{\Dagger{b_i} + θ_i^{↑/↓} + b_i^{} + θ_i^{↑/↓}}}{)}\g{.}{\vphantom{\g(\Dagger{b_i} + θ_i^{↑/↓}) + \g(b_i^{} + θ_i^{↑/↓}) - \frac{λ_i^2}{4 ω_i}} - \frac{λ_i^2}{4 ω_i}}{)}  \eqlabel{eqn:shifted-operators}\\
                    &=& \pm \frac{ε}{2} + \sum_i ω_i^{} \Dagger{\bar{b}_i} \bar{b}_i^{} - \sum_i \underbrace{\frac{λ_i^2}{4 ω_i}}_{E^{\t{g}}_i} \pns.\eqlabel{diagonal-H0-strong}
\end{eqns}
Durch das Einführen verschobener Leiteroperatoren $\bar{b}_i^{}$ und $\Dagger{\bar{b}_i}$ wird $\mathcal{H}^0_{↑/↓}$ in beiden Unterräumen von $σ_z$ diagonal.
Die Definition der neuen Leiteroperatoren bewirkt keine Änderung der Oszillatorenfrequenzen.
Ihre Wirkung auf verschobene Fockzustände $\ket{\bar{n}}$ ist
\begin{eqns}
  \bar{b} \ket{\bar{n}} &=& \sqrt{\bar{n}} \ket{\bar{n} - 1} \IEEEyesnumber \IEEEyessubnumber* \eqlabel{eqn:babar}\\
  \Dagger{\bar{b}} \ket{\bar{n}} &=& \sqrt{\bar{n} + 1} \ket{\bar{n} + 1} \pns. \eqlabel{eqn:bdaggerbar}
\end{eqns}

Das Spektrum enthält die Eigenenergien der nach rechts verschobenen harmonischen Oszillatoren im $\ket{↑}$-Unterraum von $σ_z$, wohingegen im $\ket{↓}$-Unterraum die Eigenenergien der nach links verschobenen harmonischen Oszillatoren enthalten sind.
Die Spinausrichtung impliziert also die Verschiebungsrichtung, da $σ_z$ in $\mathcal{H}_{\t{C}}$ das Vorzeichen bestimmt.

Die Verschiebungen $\g\{θ_i^{↑/↓}\}$ sind reelle Parameter, da aus \eqref{eqn:shifted-operators} $θ_i^{↑/↓} = \conj{θ_i^{↑/↓}}$ folgt.
Sie können abhängig vom Oszillator $i$ positiv oder negativ sein.
Des Weiteren hängen sie linear von der jeweiligen Kopplung ab; die Verschiebung der Oszillatoren ist somit proportional zur Kopplung.

Durch die Kopplung tritt für jeden verschobenen harmonischen Oszillator $i$ eine individuelle Grundzustandsenergieabsenkung $E^{\t{g}}_i$ auf, die unabhängig von der Verschiebungsrichtung ist, jedoch quadratisch von der Kopplung $λ_i$ abhängt.

Die Energieabsenkung $E^{\t{g}}_i$ kann durch die Badspektraldichte \eqref{eqn:bath-spectral} mit der Näherung~\eqref{eqn:stand_bath-spectral} ausgedrückt werden:
\begin{eqns}
  E^{\t{g}}_i \!\! &=& \!\! \sum_i \frac{λ_i^2}{4 ω_i} =  \int_0^{\infty} \dif{ω} \HeavisideStep(ω_{\t{c}} - ω) \PI \sum_i λ_i^2 \Diracdelta(ω - ω_i^{}) \frac{1}{4 \PI ω}
  \overset{\eqref{eqn:bath-spectral}}{=} \frac{1}{4 \PI} \int_0^{ω_{\t{c}}} \dif{ω} \frac{\f{J}(ω)}{ω} \IEEEeqnarraynumspace \IEEEyesnumber \IEEEyessubnumber*\\
  &\overset{\eqref{eqn:stand_bath-spectral}}{\approx}& \frac{α ω_{\t{c}}^{1 - s}}{2} \int_0^{ω_{\t{c}}} \dif{ω} ω^{s - 1} = \frac{α ω_{\t{c}}}{2 s} \pns.
\end{eqns}
Die Näherung \eqref{eqn:stand_bath-spectral} der Badspektralfunktion stellt für niedrige Energien also einen Zusammenhang zwischen einer diskreten Energieabsenkung $\f{E^{\t{g}}_i}(λ_i, ω_i)$ und einer kontinuierlichen Energieabsenkung $\f{E}(α)$ her.
Aus diesem Zusammenhang ergibt sich, dass die Dissipationsstärke $α$ proportional zu $λ^2$ ist und die Energieabsenkung ein Maß für die Dissipation ist.
Je stärker die Verschiebung oder je größer die Kopplung an das Bad ist, umso mehr Energie wird daher von der Störstelle in das Bad dissipiert.

Die Eigenenergien der Unterräume von $σ_z$ unterscheiden sich um den Betrag $ε$ und sind für $ε = 0$ bei gleicher Verschiebung entartet.
Die Veränderung des Parameters $ε$ in $\mathcal{H_{\t{G}}}$, beispielsweise durch ein Magnetfeld in $z$-Richtung, bewirkt also eine proportionale Änderung des Abstands der Energieniveaus aus den Unterräumen von $σ_z$.

Lediglich der interessante Fall der Entartung wird im Folgenden behandelt, in Kapitel~\ref{pertubation gaptunneling} wird $ε$ zusätzlich als Störparameter aufgefasst.
\subsubsection{Konstruktion der Eigenbasis}
  Die Eigenzustände von \eqref{diagonal-H0-strong} sind Produktzustände (siehe \cite{nolting_5.2}) aus den Spineigenzuständen $\ket{↑}$, $\ket{↓}$ und den verschobenen Fockzuständen $\ket{\bar{n}}$, sodass
  \begin{eqn}
    \ket{↑↓, \bar{n}_1, \bar{n}_2, \ldots} \in \mathscr{H}_{\t{S}, \t{B}_1, \t{B}_2, \ldots} = \mathscr{H}_\t{S} \otimes \mathscr{H}_{\t{B}_1} \otimes \mathscr{H}_{\t{B}_2} \otimes \cdots\pns,
  \end{eqn}
  wobei $\mathscr{H}_{\t{S}}$ der Spin-, $\mathscr{H}_{\t{B}_i}$ bosonische und $\mathscr{H}_{\t{S}, \t{B}_1, \t{B}_2, \ldots}$ der Spin-Boson-Hilbertraum ist.

  Für die Produktzustände gilt
  \begin{eqn}
    \braket{s', \bar{n}'_1, \bar{n}'_2, \ldots}{s, \bar{n}_1, \bar{n}_2, \ldots} = \bra{s'} \bra{\bar{n}'_1} \bra{\bar{n}'_2} \ldots \ket{\bar{n}_2} \ket{\bar{n}_1} \ket{s} = \Kroneckerdelta{_{s'}_s} \bra{\bar{n}'_1} \bra{\bar{n}'_2} \ldots \ket{\bar{n}_2} \ket{\bar{n}_1}\pns. \IEEEeqnarraynumspace
  \end{eqn}
  Die verschobenen Fockzustände $\ket{\bar{n}}$ sind bei unterschiedlicher Verschiebung nicht orthogonal.
  Numerisch günstiger als eine Fockbasis erweist sich die Verwendung einer kohärenten Basis für die verschobenen harmonischen Oszillatoren \cite{PhysRevB.81.165113}.
\subsubsection{Kohärente Zustände}
  Kohärente Zustände werden als Eigenzustände des bosonischen Absteigeoperators $b$ definiert (siehe \cite[Kap. 3.1.4]{schwabl1}):
  \begin{eqn}
    b \ket{θ} = θ \ket{θ}\pns, \;\;\; θ \in \setC\pns. \eqlabel{eqn:eigenvalue_annihilation}
  \end{eqn}
  Des Weiteren bilden sie eine übervollständige Basis \cite{bellac}.

  Der Zusammenhang zwischen kohärenten Zuständen und Fockzuständen ist durch
  \begin{eqn}
    \ket{θ} = \exp(- \frac{\abs{θ}^2}{2}) \sum_{m = 0}^{\infty} \frac{θ^m}{\sqrt{\factorial{m}}} \ket{m} \eqlabel{eqn:expansion_fock}
  \end{eqn}
  gegeben \cite[Kap. 11.2]{bellac}. Für den Überlapp zweier kohärenter Zustände folgt daher
  \begin{eqn}
    \braket{θ}{η} = \exp(- \frac{1}{2} \g(\abs{θ}^2 + \abs{η}^2)) \exp(\conj{θ}η)\pns. \eqlabel{eqn:overlap}
  \end{eqn}
  Außerdem impliziert \eqref{eqn:expansion_fock}, dass die Anregungen $m$ poissonverteilt sind \cite[Kap. 11.2]{bellac}.

  Es kann gezeigt werden, dass kohärente Zustände die Eigenzustände des verschobenen harmonischen Oszillators im Grundzustand sind, siehe zum Beispiel \cite[Kap. 3.2]{quantum-optics}.
